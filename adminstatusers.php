<?php
	//Подключение библиотек, запуск сессии 
	require_once "blocks/start.php";
	if (GetUser($_SESSION["user_id"])['Acceslevel'] < 3) {header("Location: index.php");}
	?>
	<!doctype html>
	<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
	<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		КиноДруг - Панель администратора
	</title>
	<!-- Линки -->
	<?php
		require_once "blocks/links.php"
	?>
	<!-- Конец Линков -->
	</head>
	<body id="home-page" class="template-collection ">    
	<div id="shopify-section-header" class="shopify-section">
		<!-- Навигация и заголовок -->
			<?php
				//require_once "blocks/bignav.php"
				require_once "blocks/adminnav.php"
			?>
		<!-- Моибильная навигация -->
			<?php
				"blocks/mobnavadmin.php"
			?>
		<!-- Конец мобильной шапки -->
	</div>
	<!-- Страница-->
	<main role="main" id="MainContent">
		<div class=" container ">
			<div id="test" class="fixed-action-btn toolbar">
			  <a class="btn-floating  btn-large black">
			    <i class="large material-icons">menu</i>
			  </a>
			  <ul>
			  	<li><a href="/adminstatusers.php" class="btn black"><i class="material-icons larger">people</i></a></li>
			  	<li><a href="/adminstatfilms.php" class="btn black darken-1"><i class="material-icons larger">movie</i></a></li>
			    <li><a href="/adminstatrating.php" class="btn black"><i class="material-icons larger">star_border</i></a></li>
			    <li><a href="/admin.php" class="btn black"><i class="material-icons larger">menu</i></a></li>		    
			  </ul>
			</div>
			<h5>Более подробная информация <a target="_blank" href="https://vk.com/stats?aid=6953232" class="btn-floating black"><i class="material-icons">camera_front</i></a> </h5> 
			<div class="row">
			<div class="col s12 l2">
					<div class="card">
						<h1 align="center"><?php echo GetAllCount('User'); ?></h1>
						<center>Всего</center>
					</div>
				</div>	
				<div class="col s12 l2">
					<div class="card">
						<h1 align="center"><?php echo CountUsersSex(1); ?></h1>
						<center>Женщин</center>
					</div>
				</div>				
				<div class="col s12 l2">
					<div class="card">
						<h1 align="center"><?php echo CountUsersSex(2); ?></h1>
						<center>Мужчин</center>
					</div>
				</div>				
			
				<div class="col s12 l2">
					<div class="card">
						<h1 align="center"><?php echo CountUsersAcces(1); ?></h1>
						<center>пользователей</center>
					</div>
				</div>
				<div class="col s12 l2">
					<div class="card">
						<h1 align="center"><?php echo CountUsersAcces(2); ?></h1>
						<center>модераторов</center>
					</div>
				</div>
				<div class="col s12 l2">
					<div class="card">
						<h1 align="center"><?php echo CountUsersAcces(0); ?></h1>
						<center>забанено</center>
					</div>
				</div>
			</div>
		<?php
		?>
		</div>
	</main>
	<!--Всплывающие окна-->
		<?php
			require_once "blocks/search.php"
		?> 

	<!--Конец всплавыющих окон-->
	<!--Подвал-->
		<?php
			require_once "blocks/footer.php"
		?>
	<!--Конец подвала-->
	<!-- Javascript -->
		<?php
			require_once "blocks/js.php"
		?>
			<script>
  document.getElementById("test").addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.fixed-action-btn');
    var instances = M.FloatingActionButton.init(elems, {
      toolbarEnabled: true
    });
  });

	</script>		
	<!-- Конец Javascript -->	  
	</body>
	</html>
