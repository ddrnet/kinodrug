<?php
	//Подключение библиотек, запуск сессии 
	require_once "blocks/start.php";
?>
<!doctype html>
<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		КиноДруг - <?php echo $_SESSION["user_FN"]." ".$_SESSION["user_LN"]?>
	</title>
	<!-- Линки -->
	<?php
		require_once "blocks/links.php"
	?>
	<!-- Конец Линков -->
		<link href="images/ic.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="images/ic.ico" rel="icon" type="image/x-icon" />
</head>
<body id="home-page" class="template-collection ">    
	<div id="shopify-section-header" class="shopify-section">
		<!-- Навигация и заголовок -->
			<?php
				require_once "blocks/nav.php"
			?>
		<!-- Моибильная навигация -->
			<?php
				require_once "blocks/mobnav.php"
			?>
		<!-- Конец мобильной шапки -->
		<!-- Полноэкранный поиск -->
			<?php
				require_once "blocks/fssearch.php"
			?>
		<!--Конец Полноэкранного поиска -->
	</div>
	<!-- Страница-->
	<main role="main" id="MainContent">
		<!-- история навигации -->
		<!--	<nav>
			    <div class="nav-wrapper blue">
			      	<div class="col l6">
				        <a href="index.php" class="breadcrumb">&nbsp;Главная</a>
				        <a href="user.php" class="breadcrumb"><?php echo $_SESSION["user_FN"]." ".$_SESSION["user_LN"]?></a>
				        <a href="#!" class="breadcrumb">&nbsp;Друзья</a>
			      	</div>
			    </div>
			</nav>-->
		<div class="container ">
		<!--Основная часть страницы-->		
			<!-- Описания друга -->
			<div class="card-panel grey lighten-5">
	          	<div class="row">
	          		<!-- аватарка -->
		            <div class="col s12 l4">
		            	<img src="<?php echo $_SESSION["user_photo"];?>" alt="" class="circle responsive-img"> 
		            </div>
		            <!-- ФИО -->
		            <div class="col s12 l6">
		           		<h2><?php echo $_SESSION["user_FN"]." ".$_SESSION["user_LN"]?></h2><br>
		           		<h4>Список друзей</h4>
		            </div>
	          	</div>
        	</div>
        	<div class="row">
			    <div class="col s12">
			    <!--Список друзей-->
			    <?php 
			    if(isset($_GET["num"])) $num = $_GET["num"];
			    else $num = 0;
			    PrintFriends($_SESSION["user_id"],$num);
			    PrintPages(UserFrindsCount($_SESSION["user_id"]),$num ,14,$search='');
			    ?>
		        
			    </div>
			    
        		</div>	         
			    </div>
		</div>
	</main>
	<!--Всплывающие окна-->
		<?php
			require_once "blocks/search.php"
		?>  
	<!--Конец всплавыющих окон-->
	<!--Подвал-->
		<?php
			require_once "blocks/footer.php"
		?>
	<!--Конец подвала-->
	<!-- Javascript -->
		<script>
		 	document.addEventListener('DOMContentLoaded', function() {
		    var elems = document.querySelectorAll('.fixed-action-btn');
		    var instances = M.FloatingActionButton.init(elems, options);
		  	});
		  	 	var instance2 = M.Tabs.init(el, {
                            swipeable: true
                            });
		</script>
		<?php
			require_once "blocks/js.php"
		?>
		
	<!-- Конец Javascript -->	  
</body>
</html>
