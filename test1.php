<!doctype html>
<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		КиноДруг
	</title>
	<!-- Линки -->
		

	  <link href="/css/gallery-materialize.min.css" rel="stylesheet" type="text/css" media="all" />
  <link href="/css/theme.scss.css" rel="stylesheet" type="text/css" media="all" />
	
	<!-- Lato Font -->
	<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
	<!-- Material Icons -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!-- Конец Линков -->
</head>
<body id="home-page" class="template-collection ">    
	<div id="shopify-section-header" class="shopify-section">
		<!-- Навигация и заголовок -->
			<nav class="nav-extended color  light-blue accent-3">
			<!-- фон -->
		    <div class="nav-background ">
		    	<!-- изображение -->
		    	<div class="pattern active" style="background-image: url('/images/back.png');"></div>
		    </div> 
			<div class="nav-wrapper container">
				<!-- Лого -->
			    <a href="/" itemprop="url" class="brand-logo site-logo">КиноДруг</a> 
			    <!-- Иконка для мобильной навигации -->   
			    <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
				 <!-- Меню навигации -->
			    <ul class="right hide-on-med-and-down">
			        <li >
			        	<a href="/index.php" class="site-nav__link">Главная</a>
			        </li>          		  
					<li>
			            <a href="/Friends.php" id="customer_login_link">Друзья</a>
			        </li>  		  
			        <li>
			            <a href="#" id="customer_login_link">Новинки</a>
			        </li>  	
					<li>
			            <a href="/user.php"  id="customer_search_link">Саша Кит</a>
				    </li> 
					<li>
			        <a class="fullscreen-search" href="#"><i class="material-icons">search</i></a>
			      	</li>	  		  
			    </ul>
			    <!-- Конец Меню навигации -->
			</div>
			<!-- Большая шапка -->
		   	<div class="nav-header center">
		      <h3>КиноДруг</h3>
		      <div class="tagline">Твой лучший помщник при выборе фильмов!</div>
		  	</div>
			<!-- Конец большой шапки -->
		</nav>				<!-- Моибильная навигация -->
			<ul class="side-nav" id="nav-mobile">
			<li><a href="/login.php" id="customer_login_link">Войти</a> </li>
	     	<li><a href="/reg.php" id="customer_register_link">Регистрация</a> </li>
	      	<li><a href="/" class="site-nav__link">Главная</a> </li>
	  		<li><a class="fullscreen-search" href="#">Поиск </a></li>
		  	<li><a data-target="modalS"  id="customer_search_link">Расширенный поиск</a></li>
		  	<li><a href="/contact.php" class="site-nav__link">Написать нам</a></li>
		</ul>		<!-- Конец мобильной шапки -->
		<!-- Полноэкранный поиск -->
			<div data-section-id="header" data-section-type="header-section">
			<header role="banner">
				<div class="popup-search-wrapper">
					<form action="/aftersearch.php" method="get" role="search">
						<label for="Search" class="label-hidden">
					    Какой фильм ищем?
					  	</label>
					  	<input type="search"
				         name="qsr"
				         id="Search"
				         value=""
				         placeholder="Какой фильм ищем?">
					  	<button type="submit" class="btn-flat btn-floating waves-effect">
						  	<svg aria-hidden="true" focusable="false" role="presentation" class="icon icon-search" viewBox="0 0 20 20"><path fill="#444" d="M18.64 17.02l-5.31-5.31c.81-1.08 1.26-2.43 1.26-3.87C14.5 4.06 11.44 1 7.75 1S1 4.06 1 7.75s3.06 6.75 6.75 6.75c1.44 0 2.79-.45 3.87-1.26l5.31 5.31c.45.45 1.26.54 1.71.09.45-.36.45-1.17 0-1.62zM3.25 7.75c0-2.52 1.98-4.5 4.5-4.5s4.5 1.98 4.5 4.5-1.98 4.5-4.5 4.5-4.5-1.98-4.5-4.5z"/></svg>
						    <span class="icon-fallback-text">Поиск</span>
					  	</button>
					</form>
					<i class="popup-close material-icons">close</i>
			    </div>
		  	</header>
		</div>		<!--Конец Полноэкранного поиска -->
	</div>
	<!-- Страница-->
	<main role="main" id="MainContent">
	  	<!-- Шапка сортировки-->
	  		<nav class="filter-navbar color blue">
		  	<div class="categories-wrapper">
		    	<div class="categories-container">          
		        	<ul class="categories container" data-filter="variant">
			      		<li>
			      		 	<select name="type">
								<option selected value="all">Жанр</option><option value="1">Хорор</option>
							</select>
						</li> 
						<li>  
							<form>
						        <div class="input-field">
						          	<input id="search" type="search" required>
						          	<label class="label-icon" for="search">
						          		<i class="material-icons">search</i>
						          	</label>
						         	<i class="material-icons">close</i>
						        </div>
				      		</form>
			      		</li>
			        </ul>    
		    	</div>
		 	</div>
		</nav>		<!-- Конец Шапки сортировки-->
		<div class=" container ">
		<!--Основная часть страницы-->
			<div class="gallery gallery-masonry row">
		  	<!--Список фильмов-->
				
			<!--Карта фильма-->
			<!--Теги для сортировки-->
			<div class="col s12 m4 gallery-item gallery-expand gallery-filter"
				data-type="1"
				data-section-type="product"
				data-color="1"
			  >
			<div class="gallery-curve-wrapper">
			   <!--Картинка-->
				  <a class="gallery-cover gray">
					<img 
					  src="images/1.png"
					  crossorigin="anonymous"
					  width="189" height="400"
					  data-product-featured-image />
				  </a>
					<!--Лицевая часть карты-->
				  <div class="gallery-header">
					<span style= "font-size: 20px;">Экипаж</span>
					<a class="btn-floating btn-tiny right waves-effect waves-light red accent-2"><i class="material-icons">check_box</i></a>
					<br>
					<span style= "font-size: 12px;">2016, Драма</span>
					<br>
					<a class="btn-floating btn-tiny right waves-effect waves-light red accent-3"><i class="material-icons">access_time</i></a>
					<br>
					<br>
					
				  </div>
			  <!--Тело карты-->
				<div class="gallery-body">
				  <div class="title-wrapper">
					<h3>Экипаж</h3>
				  </div>
				  <!--Атрибуты-->
					<div class="checkout-column">
						Год: 2016
						<br>Страна: Россия
						<br> Режиссер: Николай Лебедев
						<br> Жанр: драма, приключения, триллер
						<br> Dремя:	138 мин. / 02:18
						<br> Возрастной рейтинг: 6+
						<br><p class="flow-text">КиноПоиск: 7.636</p>
					</div>
				  <!--описание-->
					<div class="description">
						История талантливого молодого летчика Алексея Гущина. Он не признает авторитетов, предпочитая поступать в соответствии с личным кодексом чести. За невыполнение абсурдного приказа его выгоняют из военной авиации, и только чудом он получает шанс летать на гражданских самолетах.<br>
						Гущин начинает свою летную жизнь сначала. Его наставник — командир воздушного судна — суровый и принципиальный Леонид Зинченко. Его коллега — второй пилот, неприступная красавица Александра. Отношения складываются непросто. Но на грани жизни и смерти, когда земля уходит из-под ног, вокруг — огонь и пепел и только в небе есть спасение, Гущин показывает все, на что он способен. Только вместе экипаж сможет совершить подвиг и спасти сотни жизней.
						<br>
					<!--Траилер с ютуба-->
						<div class="video-container">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/5nlaow5dmk0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
				<div class="gallery-action">
					<a class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star</i></a>
					<a class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star</i></a>
					<a class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star</i></a>
					<a class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star</i></a>
					<a class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star_border</i></a>
				</div>  
			  </div>  
			</div>
			<!--Конец карты-->
			
			<!--Карта фильма-->
			<!--Теги для сортировки-->
			<div class="col s12 m4 gallery-item gallery-expand gallery-filter"
				data-type="1"
				data-section-type="product"
				data-color="1"
			  >
			<div class="gallery-curve-wrapper">
			   <!--Картинка-->
				  <a class="gallery-cover gray">
					<img 
					  src="images/2.png"
					  crossorigin="anonymous"
					  width="189" height="400"
					  data-product-featured-image />
				  </a>
					<!--Лицевая часть карты-->
				  <div class="gallery-header">
					<span class="title">Экипаж</span>
					<a class="btn-floating right waves-effect waves-light red accent-3"><i class="material-icons">favorite</i></a>
				  </div>
			  <!--Тело карты-->
				<div class="gallery-body">
				  <div class="title-wrapper">
					<h3>Экипаж</h3>
				  </div>
				  <!--Атрибуты-->
					<div class="checkout-column">
						Год: 2016
						<br>Страна: Россия
						<br> Режиссер: Николай Лебедев
						<br> Жанр: драма, приключения, триллер
						<br> Dремя:	138 мин. / 02:18
						<br> Возрастной рейтинг: 6+
						<br><p class="flow-text">КиноПоиск: 7.636</p>
					</div>
				  <!--описание-->
					<div class="description">
						История талантливого молодого летчика Алексея Гущина. Он не признает авторитетов, предпочитая поступать в соответствии с личным кодексом чести. За невыполнение абсурдного приказа его выгоняют из военной авиации, и только чудом он получает шанс летать на гражданских самолетах.<br>
						Гущин начинает свою летную жизнь сначала. Его наставник — командир воздушного судна — суровый и принципиальный Леонид Зинченко. Его коллега — второй пилот, неприступная красавица Александра. Отношения складываются непросто. Но на грани жизни и смерти, когда земля уходит из-под ног, вокруг — огонь и пепел и только в небе есть спасение, Гущин показывает все, на что он способен. Только вместе экипаж сможет совершить подвиг и спасти сотни жизней.
						<br>
					<!--Траилер с ютуба-->
						<div class="video-container">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/5nlaow5dmk0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
				<div class="gallery-action">
					<a class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star</i></a>
					<a class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star</i></a>
					<a class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star</i></a>
					<a class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star</i></a>
					<a class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star_border</i></a>
				</div>  
			  </div>  
			</div>
			<!--Конец карты-->
			
			<!--Карта фильма-->
			<!--Теги для сортировки-->
			<div class="col s12 m4 gallery-item gallery-expand gallery-filter"
				data-type="1"
				data-section-type="product"
				data-color="1"
			  >
			<div class="gallery-curve-wrapper">
			   <!--Картинка-->
				  <a class="gallery-cover gray">
					<img 
					  src="images/3.png"
					  crossorigin="anonymous"
					  width="189" height="400"
					  data-product-featured-image />
				  </a>
					<!--Лицевая часть карты-->
				  <div class="gallery-header">
					<span class="title">Экипаж</span>
					<a class="btn-floating right waves-effect waves-light red accent-3"><i class="material-icons">favorite</i></a>
				  </div>
			  <!--Тело карты-->
				<div class="gallery-body">
				  <div class="title-wrapper">
					<h3>Экипаж</h3>
				  </div>
				  <!--Атрибуты-->
					<div class="checkout-column">
						Год: 2016
						<br>Страна: Россия
						<br> Режиссер: Николай Лебедев
						<br> Жанр: драма, приключения, триллер
						<br> Dремя:	138 мин. / 02:18
						<br> Возрастной рейтинг: 6+
						<br><p class="flow-text">КиноПоиск: 7.636</p>
					</div>
				  <!--описание-->
					<div class="description">
						История талантливого молодого летчика Алексея Гущина. Он не признает авторитетов, предпочитая поступать в соответствии с личным кодексом чести. За невыполнение абсурдного приказа его выгоняют из военной авиации, и только чудом он получает шанс летать на гражданских самолетах.<br>
						Гущин начинает свою летную жизнь сначала. Его наставник — командир воздушного судна — суровый и принципиальный Леонид Зинченко. Его коллега — второй пилот, неприступная красавица Александра. Отношения складываются непросто. Но на грани жизни и смерти, когда земля уходит из-под ног, вокруг — огонь и пепел и только в небе есть спасение, Гущин показывает все, на что он способен. Только вместе экипаж сможет совершить подвиг и спасти сотни жизней.
						<br>
					<!--Траилер с ютуба-->
						<div class="video-container">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/5nlaow5dmk0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
				<div class="gallery-action">
					<a class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star</i></a>
					<a class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star</i></a>
					<a class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star</i></a>
					<a class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star</i></a>
					<a class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star_border</i></a>
				</div>  
			  </div>  
			</div>
			<!--Конец карты-->
					  	</div> 
			<!--Конец списка-->
		<!--Номера страниц-->
			
		<ul class="pagination">
		<li class="disabled">
			<a href="#!"><i class="material-icons">chevron_left</i></a>
		</li>
		<li class="active blue accent-3">
			<a href="/index.php?num=0">1</a>
		</li>
		<li class="disabled">
			<a href="#!"><i class="material-icons">chevron_right</i></a>
		</li>
		</ul>
		  
		<!--Конец номеров страниц-->
		</div>
	</main>
	<!--Всплывающие окна-->
		<!--Поиск-->
<div id="modalS" class="modal">
    <div class="modal-content">
	<h4>Поиск</h4>
      <form method="get" action="/aftersearch.php" id="search" accept-charset="UTF-8" class="contact-form"><input type="hidden" name="form_type" value="seacrh" /><input type="hidden" name="utf8" value="✓" /> 

    <div class="input-field col s12">
								<select name="brand[]"  multiple>
								<option value="" disabled selected>Выберие производителя:</option>
								<option value="1" selected >Lada</option><option value="2" selected >Reno</option><option value="3" selected >daewoo</option>								</select>
								<label>Производель</label>
							  </div>
							     <div class="input-field">
      <label for="modelcar">Модель автомобиля</label>
      <input type="text"
        name="modelcar"
        id="modelcar"
        value="">
    </div>
	<div class="input-field col s12">
	<select  name="color[]" multiple>
	<option value="" disabled selected>Выберие цвет:</option>
	<option value="3" selected >Белый</option><option value="2" selected >Голубой</option><option value="1" selected >Красный</option>	</select>
	<label>Цвет</label>
  </div>
	<div class="input-field col s12">
	<select name="type[]"  multiple>
	<option value="" disabled selected>Выберие тип кузова:</option>
			<option value="1" selected >хэтчбек</option>	</select>
	<label>Тип кузова</label>
	</div>
	<div class="input-field col s12">
	<select name="type_box[]"  multiple>
	<option value="" disabled selected>Выберие тип коробки передач:</option>
			<option value="1" selected >Автомат</option><option value="2" selected >Ручная</option>	</select>
	<label>Тип коробки передач</label>
	</div>
	 Стоимость дня проката 
	 <div class="row">
	<div class="input-field col s6">
    <label for="pricemin">ОТ</label>
      <input type="number"
        name="pricemin"
        id="price"
        value="1000">
    </div>
<div class="input-field col s6">
    <label for="pricemax">ДО</label>
      <input type="number"
        name="pricemax"
        id="price"
        value="1500">
    </div>
	 </div>
    <input type="submit" name="carser" class="btn" value="Найти">
  </form>  
</div>
</div>  
	<!--Конец всплавыющих окон-->
	<!--Подвал-->
		<div id="shopify-section-footer" class="shopify-section"><footer class="page-footer">
  <div class="container">
    <div class="row">         
        <div class="col s12 m4">
          <h5>Меню</h5>
          <ul>
            
              <li><a href="/index.php">Главная</a></li>
            
              <li><a href="/contact.php">Напишите нам</a></li>
            
              <li><a href="/search.php">Поиск</a></li>
            
          </ul>
        </div>
      

    </div>
  </div>
  <div class="footer-copyright">
  
    <div class="container">
	
      Жомов Юрий, Новоселова Александра, Сиютки Дмитрий, Щукин Сергей, Оруджев Ибрагим<small>, Copyright &copy; 201, MaterializeCSS </small>
    </div>
  </div>



</div>	<!--Конец подвала-->
	<!-- Javascript -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

	  <script src="//cdn.shopify.com/s/assets/themes_support/option_selection-ea4f4a242e299f2227b2b8038152223f741e90780c0c766883939e8902542bda.js" type="text/javascript"></script>

	  <script src="//cdn.shopify.com/s/assets/themes_support/api.jquery-0ea851da22ae87c0290f4eeb24bc8b513ca182f3eb721d147c009ae0f5ce14f9.js" type="text/javascript"></script>

	  <!--[if (gt IE 9)|!(IE)]><!--><script src="//cdn.shopify.com/s/files/1/2030/2737/t/6/assets/vendor.js?7525450747913588508" defer="defer"></script><!--<![endif]-->
	  <!--[if lt IE 9]><script src="//cdn.shopify.com/s/files/1/2030/2737/t/6/assets/vendor.js?7525450747913588508"></script><![endif]-->

	  <!--[if (gt IE 9)|!(IE)]><!--><script src="js/theme.js" defer="defer"></script><!--<![endif]-->
	  <!--[if lt IE 9]><script src="//cdn.shopify.com/s/files/1/2030/2737/t/6/assets/theme.js?7525450747913588508"></script><![endif]-->

	  
	    <style>
	      .gallery .gallery-item:nth-child(n + 7) {
	        display: none;
	      }
		    </style>
			 <script>
	      $(document).ready(function() {
	        var pagination_limit =100;
	        console.log("PAGE LIMIT", pagination_limit);
	        var $all_products = $('.gallery .gallery-item');
	        var $current_products = $all_products;
	        var page_max = Math.floor($current_products.length / pagination_limit) + 1;
	        var page_number = getUrlParameter('page') || 1;
	        var $shown_products = getPaginated($current_products, page_number, pagination_limit);
	        var $masonry = $('.gallery');
	        $masonry.find('.gallery-item:nth-child(n +'+($shown_products.length+1)+')').remove();
	        renderPagination($('.gallery'));

	        // Get Url parameter
	        function getUrlParameter(sParam) {
	          var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	            sURLVariables = sPageURL.split('&'),
	            sParameterName,
	            i;

	          for (i = 0; i < sURLVariables.length; i++) {
	            sParameterName = sURLVariables[i].split('=');

	            if (sParameterName[0] === sParam) {
	              return sParameterName[1] === undefined ? true : sParameterName[1];
	            }
	          }
	        }

	        // Slice products with respect to pagination
	        function getPaginated(products, page, limit) {
	          var start = (page - 1) * limit;
	          var end = page * limit;
	          return products.slice(start, end);
	        }

	        // Render pagination elements
	        function renderPagination(gallery) {
	          page_max = Math.floor($current_products.length / pagination_limit) + 1;
	          var $pagination = $('<ul lass="pagination"></ul>');
	          var left = $('<li ata-paginate="left"><a><i class="material-icons">chevron_left</i></a></li>');
	          var right = $('<li ata-paginate="right"><a><i class="material-icons">chevron_right</i></a></li>');

	          // Add .container if dark_theme
	          if (false) {
	            $pagination.addClass('container');
	          }
	          
	          // If only 1 page, don't render
	          if (page_max === 1) {
	            gallery.next('l.pagination').remove();
	            return;
	          }

	          if (page_number === 1) {
	            left.addClass('disabled');
	          } else if (page_number === page_max) {
	            right.addClass('disabled');
	          }

	          $pagination.append(left);
	          for (var i = 0; i < page_max; i++) {
	            var curr_page_number = i + 1;
	            var $page = $('<li dat-paginate="' + curr_page_number + '"><a>' + curr_page_number + '</a></li>');
	            if (curr_page_number === page_number) {
	              $page.addClass('active');
	            } else {
	              $page.addClass('waves-effect');
	            }
	            $pagination.append($page);
	          }
	          $pagination.append(right);

	          if (gallery.next('ul.pagiation').length) {
	            gallery.next('ul.pagiation').replaceWith($pagination);
	          } else {
	            gallery.after($pagination);
	          }

	          $pagination.find('li:not(.disabled):not(.active)').on('click', function() {
	            var data_paginate = $(this).attr('data-paginate');
	            if (data_paginate === 'left') {
	              changePage(page_number - 1);
	            } else if (data_paginate === 'right') {
	              changePage(page_number + 1);
	            } else {
	              changePage(parseInt(data_paginate));
	            }
	          });
	        }

	        // Render products, reinit galleryExpand, render pagination
	        var renderProducts = function(gallery, added, removed) {
	          gallery
	            .masonry('remove', removed)
	            .masonry('layout');

	          gallery
	            .append(added)
	            .masonry('appended', added)
	            .masonry('layout');

	          gallery.find('.gallery-expand').galleryExpand({
	            onShow: Window.GalleryExpandOnShow,
	            dynamicRouting: true
	          });


	          // Reinit SPR
	          if (SPR) {
	            SPR.initDomEls();
	            SPR.loadBadges();
	          }
	          
	          renderPagination(gallery);
	        };

	        // Change page and handle masonry
	        var changePage = function(page) {
	          page_number = page;
	          $shown_products = getPaginated($current_products, page, pagination_limit);

	          renderProducts($masonry, $shown_products, $masonry.find('.gallery-item'));
	        };

	        var categories = $('nav .categories-container');
	        if (categories.length) {
	          function filterProducts(link) {
	            console.log("FILTER PRODUCTS", link.parent('.categories').attr('data-filter'));

	            var type_selector = '.gallery-item';

	            // Change active of button if not variant (select fields)
	            if (link.parent('.categories').attr('data-filter') !== 'variant') {
	              link.parent().find('li').removeClass('active');
	              link.addClass('active');
	            }

	            categories.find('.categories').each(function() {
	              // Generate type selector for variants (select fields)
	              if ($(this).attr('data-filter') === 'variant') {
	                  var selects = $(this).find('select');
	                  selects.each(function() {
	                    var val = $(this).val();
	                    if (val !== 'all') {
	                      var filter_type = $(this).attr('name');
	                      type_selector += '[data-' + filter_type + '="' + val + '"]';
	                    }
	                  });

	              // Generate type selector for everything else (hash based)
	              } else {

	                console.log("FIND HASH", $(this));
	                var hash = $(this).find('li.active > a').first()[0].hash.substr(1);
	                if (hash !== 'all') {
	                  var filter_type = $(this).attr('data-filter');
	                  type_selector += '[data-' + filter_type + '="' + hash + '"]';
	                }
	              }
	            });

	            console.log("FILTER", type_selector);

	            page_number = 1;
	            var $removed_products = $remaining_products = $added_products = $();

	            $removed_products = $shown_products.not(type_selector);
	            $remaining_products = $shown_products.filter(type_selector);
	            $current_products = $all_products.filter(type_selector);
	            $shown_products = getPaginated($current_products, page_number, pagination_limit);
	            $added_products = $shown_products.not($remaining_products);
	            $removed_products = $removed_products.add($remaining_products.not($shown_products));

	            console.log($removed_products, $remaining_products, $shown_products, $added_products);

	            renderProducts($masonry, $added_products, $removed_products);
	          }

	          // Add select onchange handler for variant filters
	          if (categories.find('.categories[data-filter="variant"]')) {
	            $(document).on('change', '.categories-container .categories select', function(e) {
	              console.log("CHANGE", $(e.target));
	              filterProducts($(e.target).closest('li'));
	            });
	          }

	          // Add click handler for other filters
	          var $links = categories.find('li');
	          $links.each(function() {
	            var $link = $(this);
	            $link.on('click', function() { filterProducts($link); });
	          });
	        }

	      });
	    </script>
	  

	  <script>
	    $(document).ready(function() {
	      var categories = $('nav .categories-container');
	      if (categories.length) {
	        categories.pushpin({ top: categories.offset().top });
	      }
	    });
	  </script>	<!-- Конец Javascript -->	  
<div style="text-align: right;position: fixed;z-index:9999999;bottom: 0;width: auto;right: 1%;cursor: pointer;line-height: 0;display:block !important;"><a title="Hosted on free web hosting 000webhost.com. Host your own website for FREE." target="_blank" href="https://www.000webhost.com/?utm_source=000webhostapp&utm_campaign=000_logo&utm_medium=website&utm_content=footer_img"><img src="https://cdn.000webhost.com/000webhost/logo/footer-powered-by-000webhost-white2.png" alt="www.000webhost.com"></a></div><script>function getCookie(e){for(var t=e+"=",n=decodeURIComponent(document.cookie).split(";"),o=0;o<n.length;o++){for(var i=n[o];" "==i.charAt(0);)i=i.substring(1);if(0==i.indexOf(t))return i.substring(t.length,i.length)}return""}getCookie("hostinger")&&(document.cookie="hostinger=;expires=Thu, 01 Jan 1970 00:00:01 GMT;",location.reload());var notification=document.getElementsByClassName("notice notice-success is-dismissible"),hostingerLogo=document.getElementsByClassName("hlogo"),mainContent=document.getElementsByClassName("notice_content")[0],newList=["Powerful and Easy-To-Use Control Panel.","1-Click Auto Installer and 24/7 Live Support.","Free Domain, Email and SSL Bundle.","5x faster WordPress performance","Weekly Backups and Fast Response Time."];if(0<notification.length&&null!=mainContent){var googleFont=document.createElement("link");googleFontHref=document.createAttribute("href"),googleFontRel=document.createAttribute("rel"),googleFontHref.value="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600",googleFontRel.value="stylesheet",googleFont.setAttributeNode(googleFontHref),googleFont.setAttributeNode(googleFontRel);var css="@media only screen and (max-width: 768px) {.web-hosting-90-off-image-wrapper {position: absolute;} .notice_content {justify-content: center;} .web-hosting-90-off-image {opacity: 0.3;}} @media only screen and (min-width: 769px) {.notice_content {justify-content: space-between;} .web-hosting-90-off-image-wrapper {padding: 0 5%}} .content-wrapper {z-index: 5} .notice_content {display: flex; align-items: center;} * {-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale;} .upgrade_button_red_sale{border: 0; border-radius: 3px; background-color: #ff123a !important; padding: 15px 55px !important; margin-left: 30px; font-family: 'Open Sans', sans-serif; font-size: 16px; font-weight: 600; color: #ffffff;} .upgrade_button_red_sale:hover{color: #ffffff !important; background: #d10303 !important;}",style=document.createElement("style"),sheet=window.document.styleSheets[0];style.styleSheet?style.styleSheet.cssText=css:style.appendChild(document.createTextNode(css)),document.getElementsByTagName("head")[0].appendChild(style),document.getElementsByTagName("head")[0].appendChild(googleFont);var button=document.getElementsByClassName("upgrade_button_red")[0],link=button.parentElement;link.setAttribute("href","https://www.hostinger.com/hosting-starter-offer?utm_source=000webhost&utm_medium=panel&utm_campaign=000-wp"),link.innerHTML='<button class="upgrade_button_red_sale">TRANSFER NOW</button>',(notification=notification[0]).setAttribute("style","padding-bottom: 10px; padding-top: 5px; background-image: url(https://cdn.000webhost.com/000webhost/promotions/springsale/mountains-neon-background.jpg); background-color: #000000; background-size: cover; background-repeat: no-repeat; color: #ffffff; border-color: #ff123a; border-width: 8px;"),notification.className="notice notice-error is-dismissible",(hostingerLogo=hostingerLogo[0]).setAttribute("src","https://cdn.000webhost.com/000webhost/promotions/springsale/logo-hostinger-white.svg"),hostingerLogo.setAttribute("style","float: none !important; height: auto; max-width: 100%; margin: 40px 20px 10px 30px;");var h1Tag=notification.getElementsByTagName("H1")[0];h1Tag.remove();var paragraph=notification.getElementsByTagName("p")[0];paragraph.innerHTML="Migrate your WordPress to Hostinger and enjoy the best features on the market! The time is now!",paragraph.setAttribute("style",'max-width: 600px; margin-left: 30px; font-family: "Open Sans", sans-serif; font-size: 16px; font-weight: 600;');var list=notification.getElementsByTagName("UL")[0];list.setAttribute("style","max-width: 675px;");for(var listElements=list.getElementsByTagName("LI"),i=0;i<newList.length;i++)listElements[i].setAttribute("style","color:#ffffff; list-style-type: disc; margin-left: 30px; font-family: 'Open Sans', sans-serif; font-size: 14px; font-weight: 300; line-height: 1.5;"),listElements[i].innerHTML=newList[i];listElements[listElements.length-1].remove();var org_html=mainContent.innerHTML,new_html='<div class="content-wrapper">'+mainContent.innerHTML+'</div><div class="web-hosting-90-off-image-wrapper"><img class="web-hosting-90-off-image" src="https://cdn.000webhost.com/000webhost/promotions/springsale/web-hosting-90-off.png"></div>';mainContent.innerHTML=new_html;var saleImage=mainContent.getElementsByClassName("web-hosting-90-off-image")[0];!function(){var t=document.querySelectorAll("body.wp-admin")[0];function e(){var e=document.createElement("iframe");e.id="hgr-promo-widget",e.setAttribute("src","https://www.hostinger.com/widgets/bottom-banner-sale/000_wp_admin"),e.setAttribute("allowfullscreen",!0),e.setAttribute("frameborder",0),e.style.cssText="z-index: 2147483000 !important;position: fixed !important;bottom: 0; width: 100%;!important; left: 0!important;",e.style.opacity=0,e.onload=function(){iFrameResize({},"#hgr-promo-widget"),e.style.opacity=1},t.insertAdjacentElement("afterend",e)}if(window.iFrameResize)e();else{var n=document.createElement("script");n.type="text/javascript",t.insertAdjacentElement("afterend",n),n.onload=e,n.src="https://unpkg.com/iframe-resizer@3.6.3/js/iframeResizer.min.js"}}()}</script></body>
</html>

