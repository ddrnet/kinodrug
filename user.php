<?php
	//Подключение библиотек, запуск сессии 
	require_once "blocks/start.php";
?>
<!doctype html>
<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		КиноДруг - <?php echo $_SESSION["user_FN"]." ".$_SESSION["user_LN"]?>
	</title>
	<!-- Линки -->
	<?php
		require_once "blocks/links.php"
	?>
	<script src="https://vk.com/js/api/openapi.js?160" type="text/javascript"></script>
	<link href="images/ic.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="images/ic.ico" rel="icon" type="image/x-icon" />
	<!-- Конец Линков -->
</head>
<body id="home-page" class="template-collection ">    
<?php
			require_once "blocks/js.php"
		?>
	<div id="shopify-section-header" class="shopify-section">
		<!-- Навигация и заголовок -->
			<?php
				require_once "blocks/nav.php"
			?>
		<!-- Моибильная навигация -->
			<?php
				require_once "blocks/mobnav.php"
			?>
		<!-- Конец мобильной шапки -->
		<!-- Полноэкранный поиск -->
			<?php
				require_once "blocks/fssearch.php"
			?>
		<!--Конец Полноэкранного поиска -->
	</div>
	<!-- Страница-->
	<main role="main" id="MainContent">
		<!-- история навигации -->
		<!-- 	<nav>
			    <div class="nav-wrapper blue">
			      	<div class="col l6">
				        <a href="index.php" class="breadcrumb">&nbsp;Главная</a>
				        <a href="#!" class="breadcrumb"><?php echo $_SESSION["user_FN"]." ".$_SESSION["user_LN"]?></a>
			      	</div>
			    </div>
			</nav>-->
		<div class="container ">
		<!--Основная часть страницы-->		
			<!-- Описания друга -->
			<div class="card-panel grey lighten-5">
	          	<div class="row">
	          		<!-- аватарка -->
		            <div class="col s12 l4">
		            	<img src="<?php echo $_SESSION["user_photo"];?>" alt="" class="circle responsive-img"> 
		            </div>
		            <!-- ФИО -->
		            <div class="col s12 l6">
		           		<h2><?php echo $_SESSION["user_FN"]." ".$_SESSION["user_LN"]?></h2><br>
		           		<h5>Информация о профиле</h5>
		           		<?php 
		           		   /* $U = GetUser($_SESSION["user_id"]);
		           		    if ($U['Acceslevel'] <= 0) echo '<br>Забанен';
		           		    else if ($U['Acceslevel'] > 0 && $U['Acceslevel'] == 1) echo '<br>Пользователь';
		           		    else if ($U['Acceslevel'] > 0 && $U['Acceslevel'] == 2) echo '<br>Модератор';
		           		    else if ($U['Acceslevel'] > 0 && $U['Acceslevel'] == 3) echo '<br>Администратор';
		           		    else if ($U['Acceslevel'] > 0 && $U['Acceslevel'] == 4) echo '<br>Разработчик';*/
		           		    echo '<p style=" font-size: 17px">Оцененных фильмов: '.UsersFilmsLimCount($_SESSION["user_id"]);
		           		    echo '<br>Друзей: '.UserFrindsCount($_SESSION["user_id"]).'</p>';
		           		?>
		            </div>
	          	</div>
        	</div>
        	<div class="row">
			    <div class="col s12">
			      <ul class="tabs tabs-fixed-width tab-demo">
			        <li class="tab col s3"><a href="#test1" style="font-family: Lucida Grande !important;font-size: 18px !important;
			        font-variant: normal !important;
			        font-style: normal !important;
			        color: #7a2174 !important;">Мои оценки</a></li>
			        <li class="tab col s3"><a href="#test2"style="font-family: Lucida Grande !important;font-size: 18px !important;
			        font-variant: normal !important;
			        font-style: normal !important;
			        color: #7a2174 !important;">Мои друзья</a></li>
			        <li class="tab col s3"><a href="#test3"style="font-family: Lucida Grande !important;font-size: 18px !important;
			        font-variant: normal !important;
			        font-style: normal !important;
			        color: #7a2174 !important;">Мои жанры</a></li>
			        
			      </ul>
			    </div>
			    <div id="test1" class="col s12">
			    	<div class="gallery gallery-masonry row">
				  	<!--Список фильмов-->
						<?php
						$Films = UsersFilmsLim($_SESSION["user_id"],0,6);
							films($Films);
						?>
					
				  	</div> 
				  		<a href="Likes.php?user=<?php echo $_SESSION["user_id"] ?>" class="waves-effect waves-light btn">Полный список</a>
				</div>
			    <div id="test2" class="col s12">
			    <!--Список друзей-->
			    <?php PrintFriends($_SESSION["user_id"]); ?>
			    <a href="Friends.php" class="waves-effect waves-light btn">Полный список</a>
			    </div>
			    <div id="test3" class="col s12">
			    <!--Список жанров-->
			    <form enctype="multipart/form-data" method="post" action=""  accept-charset="UTF-8">	
			    <div class="card-panel grey lighten-5">
		          	<div class="row">
		          		<h3>Любимые жанры:</h3>
		          	 <?php 
		          	 	if (!empty($_POST['Genre'])) 
		          	 	{
                           	if (!empty($_POST['Jenre']))  $Jenres = $_POST['Jenre'];
                           	else $Jenres = [];
                            $tosay = AddPref($_SESSION["user_id"],$Jenres);
		          	 	}
		          	 $Genre = GetAll("Jenre");
                    echo '<div class="row">
		          		<div class="col s12 l6">';
		          		if (count($Genre)%2 == 1) $med = count($Genre)/2+1;
                	    else $med = count($Genre)/2;
                	    for($i = 0; $i < count($Genre)/2; $i++)
                        {
                            if(CheckUserGenre($_SESSION["user_id"],$Genre[$i]["entry"]))
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$i]["entry"].'" id="Jenre'.$i.'"  checked />
		      			    <label for="Jenre'.$i.'">'.$Genre[$i]["Name"].'</label>  
		      			    <br>';
                            }
                            else 
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$i]["entry"].'" id="Jenre'.$i.'" />
		      			    <label for="Jenre'.$i.'">'.$Genre[$i]["Name"].'</label>  
		      			    <br>';  
                            }
                        }
                    echo '</div>';
                    echo '<div class="col s12 l6">';
                        for ($i = $med; $i < count($Genre); $i++)
                        {
                            if(CheckUserGenre($_SESSION["user_id"],$Genre[$i]["entry"]))
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$i]["entry"].'" id="Jenre'.$i.'" checked />
		      			    <label for="Jenre'.$i.'">'.$Genre[$i]["Name"].'</label>  
		      			    <br>';
                            }
                            else 
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$i]["entry"].'" id="Jenre'.$i.'" />
		      			    <label for="Jenre'.$i.'">'.$Genre[$i]["Name"].'</label>  
		      			    <br>';  
                            }
                        }
                    echo '</div></div>
                    <br>

                     <input type="submit" name="Genre" class="btn" value="Сохранить изменения">
                    </div>';
                    ?>
		          	</div>
        		</div>	         
			    </form>        
			    </div>
			
		  	</div>
		</div>
	</main>
	<!--Всплывающие окна-->
		<?php
			require_once "blocks/search.php"
		?> 
		<?php
			    if (!empty($_POST['Genre'])) {
                    
			         echo "<script>function ready() {
                     Materialize.toast('".$tosay."', 4000);
                     }
                     document.addEventListener(\"DOMContentLoaded\", ready);</script>";
			       
			        }
			?>
	<!--Конец всплавыющих окон-->
	<!--Подвал-->
		<?php
			require_once "blocks/footer.php"
		?>
	<!--Конец подвала-->
	<!-- Javascript -->
		<script>
		 	document.addEventListener('DOMContentLoaded', function() {
		    var elems = document.querySelectorAll('.fixed-action-btn');
		    var instances = M.FloatingActionButton.init(elems, options);
		  	});
		  	 	var instance2 = M.Tabs.init(el, {
                            swipeable: true
                            });
		</script>
		
		
	<!-- Конец Javascript -->	  
</body>
</html>
