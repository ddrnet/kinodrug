<?php
	//Подключение библиотек, запуск сессии 
	require_once "blocks/start.php";
	if (GetUser($_SESSION["user_id"])['Acceslevel'] < 3) {header("Location: index.php");}

?>
<!doctype html>
<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		КиноДруг - Фильмы
	</title>
	<!-- Линки -->
	<?php
		require_once "blocks/links.php"
	?>
	<!-- Конец Линков -->
</head>
<body id="home-page" class="template-collection ">    
	<div id="shopify-section-header" class="shopify-section">
		<!-- Навигация и заголовок -->
			<?php
				require_once "blocks/adminnav.php"
			?>
		<!-- Моибильная навигация -->
			<?php
				require_once "blocks/mobnavadmin.php"
			?>
		<!-- Конец мобильной шапки -->
		<!-- Полноэкранный поиск -->
			<?php
				require_once "blocks/fssearchadfilm.php";
			?>
		<!--Конец Полноэкранного поиска -->
	</div>
	<!-- Страница-->
	<main role="main" id="MainContent">
	    	<?php
		  	if (!empty($_POST['ADD'])) {
              $Jenres = $_POST['Jenre'];
              if(empty($Jenres)) 
              {
                $tosay = 'Выберите хотя бы один жанр!';
              } 
              else
              {
                $uploaddir = 'films/';
        		$uploadfile = $uploaddir . basename($_FILES['poster']['name']);
        		if (!move_uploaded_file($_FILES['poster']['tmp_name'], $uploadfile))
        		{
        			$uploadfile = $_POST[path];
        			if ($uploadfile != "")
        			{	
        			$uploadfile = $_POST[path];
        			}
        			else {$uploadfile = 'images/default.jpg';}
        		}
        		if (!empty($_POST['NAME']) && !empty($_POST['text']))
        		{
            		if (empty($_POST['date'])) $date = '0000-00-00';
            		else $date = $_POST['date'];
            		if (empty($_POST['Country'])) $country = 'null';
            		else $country = "'".$_POST['Country']."'";
            		if (empty($_POST['budget'])) $budget = 'null';
            		else $budget = "'".$_POST['budget']."'";
            		if (empty($_POST['fees'])) $fees = 'null';
            		else $fees = "'".$_POST['fees']."'";
            		if (empty($_POST['rating'])) $rating = 'null';
            		else $rating = "'".$_POST['rating']."'";
            		if (empty($_POST['time'])) $time = 'null';
            		else $time = "'".$_POST['time']."'";
            		if (empty($_POST['Agerating'])) $Agerating = 'null';
            		else $Agerating = "'".$_POST['Agerating']."'";
                    $tosay = AddFilm($_POST['NAME'], $date , $country, $budget, $fees, $rating, $time, $Agerating, $uploadfile, $_POST['text'],$Jenres);
            	}
              }
		  	}
		  	else if (!empty(($_POST['edit']))) {
              $Jenres = $_POST['Jenre'];
              if(empty($Jenres)) 
              {
                $tosay = 'Выберите хотя бы один жанр!';
              } 
              else
              {
                $uploaddir = 'films/';
        		$uploadfile = $uploaddir . basename($_FILES['poster']['name']);
        		if (!move_uploaded_file($_FILES['poster']['tmp_name'], $uploadfile))
        		{
        			$uploadfile = $_POST["path"];
        			if ($uploadfile != "")
        			{	
        			$uploadfile = $_POST["path"];
        			}
        			else {$uploadfile = 'images/default.jpg';}
        		}
        		if (!empty($_POST['FilmName']) && !empty($_POST['Description']))
        		{
        		   
            		if (empty($_POST['DateOf'])) $date = '0000-00-00';
            		else $date = $_POST['DateOf'];
            		if (empty($_POST['Country'])) $country = 'null';
            		else $country = "'".$_POST['Country']."'";
            		if (empty($_POST['Budget'])) $budget = 'null';
            		else $budget = "'".$_POST['Budget']."'";
            		if (empty($_POST['Fees'])) $fees = 'null';
            		else $fees = "'".$_POST['Fees']."'";
            		if (empty($_POST['KinoRating'])) $rating = 'null';
            		else $rating = "'".$_POST['KinoRating']."'";
            		if (empty($_POST['film_time'])) $time = 'null';
            		else $time = "'".$_POST['film_time']."'";
            		if (empty($_POST['AgeRating'])) $Agerating = 'null';
            		else $Agerating = "'".$_POST['AgeRating']."'";
                    $tosay = editFilm($_POST['entry'], $_POST['FilmName'],$uploadfile, $date, $budget,$fees, $rating,$time,$Agerating, $country, $_POST['Description'],$Jenres);
            	}
              }
		  	}
            ?>
		<div class="container ">
		<!--Основная часть страницы-->		
        	<div class="row">
			    <div class="col s12">
			        <h2>Список Фильмов</h2>
			        	<script src="ckeditor/ckeditor.js"></script>
    			    <?php 
    			    if(isset($_GET["num"])) {$num = $_GET["num"];}
					else {$num = 0;}
					$per_page = 14;
                if (!empty($_POST["DEL"]))
                {
                    $tosay = deletefilm($_POST["entry"]);
                }
                 $search = "";
		        if(!empty($_GET["qsr"]))
		        {
		             $search = "&qsr=".$_GET["qsr"];
		             $Films = FindNameAdminFilm($num, $per_page, $_GET["qsr"]);
		             $FCount = FindNameAdminFilmCount($_GET["qsr"]);
		             if ($FCount == 0) echo "<h4>Ничего не найдено!</h4>";
		        }
		        else
		        {
		            $Films = GetSome('Film', $num, $per_page);
		            $FCount = GetAllCount('Film');
		        }
    			    PrintFilms($Films);
    			    PrintPages($FCount,/*GetAllCount('Film')*/$num ,$per_page,$search);
    			    ?>
			    </div>
        	</div>	         
		</div>
		<div id="modalADD" class="modal">
			<form enctype="multipart/form-data" method="post" action=""  accept-charset="UTF-8">
			    <div class="modal-content">
				    <label for="NAME">Название фильма</label>
                    <input type="text" placeholder="Название фильма" name="NAME" id="NAME" value="">
                    <label for="date">Дата выхода в прокат</label>
                    <input type="date" name="date" id="date" value="">
                    <label for="budget">Бюджет фильма</label>
                    <input type="number" name="budget" id="budget" value="">
                    <label for="fees">Кассовые сборы</label>
                    <input type="number" name="fees" id="fees" value="">
                    <label for="rating">Оценка критиков</label>
                    <input type="number" name="rating" id="rating" value="">
                    <label for="time">Продолжительность</label>
                    <input type="time" name="time" id="time" value="">
                    <label for="Agerating">Возрастное ограничение</label>
                    <input type="number" name="Agerating" id="Agerating" value="">
                    <label for="Country">Страна</label>
                    <input type="text"  name="Country" id="Country" value="">
                    <?php $Genre = GetAll("Jenre");
                       echo '<h4>Жанры:</h4><div class="row">
					   <div class="col s12 l6">';

		          		if (count($Genre)%2 == 1) $med = count($Genre)/2+1;
                	    else $med = count($Genre)/2;
                	    for($i = 0; $i < count($Genre)/2; $i++)
                        {
                            if(CheckUserGenre($_SESSION["user_id"],$Genre[$i]["entry"]))
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$i]["entry"].'" id="Jenre'.$i.'"  checked />
		      			    <label for="Jenre'.$i.'">'.$Genre[$i]["Name"].'</label>  
		      			    <br>';
                            }
                            else 
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$i]["entry"].'" id="Jenre'.$i.'" />
		      			    <label for="Jenre'.$i.'">'.$Genre[$i]["Name"].'</label>  
		      			    <br>';  
                            }
                        }
                    echo '</div>';
                    echo '<div class="col s12 l6">';
                        for ($i = $med; $i < count($Genre); $i++)
                        {
                            if(CheckUserGenre($_SESSION["user_id"],$Genre[$i]["entry"]))
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$i]["entry"].'" id="Jenre'.$i.'" checked />
		      			    <label for="Jenre'.$i.'">'.$Genre[$i]["Name"].'</label>  
		      			    <br>';
                            }
                            else 
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$i]["entry"].'" id="Jenre'.$i.'" />
		      			    <label for="Jenre'.$i.'">'.$Genre[$i]["Name"].'</label>  
		      			    <br>';  
                            }
                        }
                  
                    echo '</div></div>';
                    
                    ?>
                    <div class="file-field input-field">
                      <div class="btn">
                        <span>Выбрать изображение</span>
                        <input type="file"name="poster"
                		accept="image/*" 
                        id="poster">
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate" placeholder="Постер" type="text">
                      </div>
                    </div>
				    <label for="text">Описание</label>
                    <textarea name="text" id="editor" class="materialize-textarea"></textarea>
                    <br>
                    <input type="submit" name="ADD" class="btn" value="Добавить">
                </div>
			   </form>
			    <script>
					 CKEDITOR.replace( 'editor' );
					</script>
         </div> 
        <div id="modalSearch" class="modal">
           <form enctype="multipart/form-data" method="get" action=""  accept-charset="UTF-8">
           <div class="modal-content">
            <h4>Поиск</h4>
        	<label for="NAME">Что ищем?</label>
            <input type="text" name="qsr" id="qsr" value="">
            </div>
            <div class="modal-footer">
             <input type="submit" name="SEARCH" class="modal-close btn waves-effect white waves-green btn-flat" value="Искать">
             <a href="#!" class="modal-close waves-effect waves-red btn-flat">Отмена</a>
            </div>
            </form>
         </div>  
         
		</div>
		  	<!--Плавающая кнопка поиска-->
	    <div class="fixed-action-btn">
			    <a class="btn-floating btn-large red">
                  <i class="large material-icons">menu</i>
                </a>
                <ul>
                  <li><a class="btn-floating btn-large red" href="#modalADD"><i class="large material-icons">add</i></a></li>
                  <li><a class="btn-floating btn-large black" href="#modalSearch"><i class="large material-icons">search</i></a></li>
                </ul>
			</div>
		</div>
	</main>
	<!--Всплывающие окна-->
		<?php
		if (!empty($_POST['ADD']) || !empty($_POST['DEL']) || !empty($_POST['edit'])) {
                    
			         echo "<script>function ready() {
                     Materialize.toast('".$tosay."', 4000);
                     }
                     document.addEventListener(\"DOMContentLoaded\", ready);</script>";
			       
			        }
			require_once "blocks/search.php"
		?>  
	<!--Конец всплавыющих окон-->
	<!--Подвал-->
		<?php
			require_once "blocks/footer.php"
		?>
	<!--Конец подвала-->
	<!-- Javascript -->
		<script>
		 	document.addEventListener('DOMContentLoaded', function() {
		    var elems = document.querySelectorAll('.fixed-action-btn');
		    var instances = M.FloatingActionButton.init(elems, options);
		  	});
		  	 	var instance2 = M.Tabs.init(el, {
                            swipeable: true
                            });
		</script>
		<?php
			require_once "blocks/js.php"
		?>
		
	<!-- Конец Javascript -->	  
</body>
</html>