<?php
	//Подключение библиотек, запуск сессии 
	require_once "blocks/start.php";
	if (GetUser($_SESSION["user_id"])['Acceslevel'] < 3) {header("Location: index.php");}
?>
<!doctype html>
<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		КиноДруг - Лента комментариев
	</title>
	<!-- Линки -->
	<?php
		require_once "blocks/links.php"
	?>
	<!-- Конец Линков -->
	<script src="https://vk.com/js/api/openapi.js?160" type="text/javascript"></script>
	<link href="images/Jamespeng-Movie-Trailer.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="images/Jamespeng-Movie-Trailer.ico" rel="icon" type="image/x-icon" />
</head>
<body id="home-page" class="template-collection ">    
	<div id="shopify-section-header" class="shopify-section">
		<!-- Навигация и заголовок -->
			<?php
				require_once "blocks/adminnav.php"
			?>
		<!-- Моибильная навигация -->
			<?php
				require_once "blocks/mobnavadmin.php"
			?>
		<!-- Конец мобильной шапки -->
	</div>
	<!-- Страница-->
	<main role="main" id="MainContent">
		<div class="container ">
		<!--Основная часть страницы-->		
        	<div class="row">
			    <div class="col s12">
			        <h2>Лента комментариев</h2>
    			    <div id="vk_comments_browse"></div>
                    <script type="text/javascript">
                    window.onload = function () {
                     VK.init({apiId: 6953232, onlyWidgets: true});
                     VK.Widgets.CommentsBrowse('vk_comments_browse', {limit: 100, mini: 0});
                    }
                    </script>
                    <br>
			    </div>
        	</div>	         
		</div>
		</div>
	</main>
	<!--Всплывающие окна-->
		<?php
			require_once "blocks/search.php"
		?>  
	<!--Конец всплавыющих окон-->
	<!--Подвал-->
		<?php
			require_once "blocks/footer.php"
		?>
	<!--Конец подвала-->
	<!-- Javascript -->
		<script>
		 	document.addEventListener('DOMContentLoaded', function() {
		    var elems = document.querySelectorAll('.fixed-action-btn');
		    var instances = M.FloatingActionButton.init(elems, options);
		  	});
		  	 	var instance2 = M.Tabs.init(el, {
                            swipeable: true
                            });
		</script>
		<?php
			require_once "blocks/js.php"
		?>
		
	<!-- Конец Javascript -->	  
</body>
</html>
