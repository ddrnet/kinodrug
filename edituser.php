<?php
//Подключение библиотек, запуск сессии 
	require_once "blocks/start.php";
?>
<!doctype html>
<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		КиноДруг
	</title>
	<!-- Линки -->
	<?php
		require_once "blocks/links.php"
	?>
	<!-- Конец Линков -->
</head>
<body id="home-page" class="template-collection ">    
	<div id="shopify-section-header" class="shopify-section">
		<!-- Навигация и заголовок -->
			<?php
			//	require_once "blocks/bignav.php"
				require_once "blocks/adminnav.php"
			?>
		<!-- Моибильная навигация -->
			<?php
			//	require_once "blocks/mobnav.php"
			?>
		<!-- Конец мобильной шапки -->
		<!-- Полноэкранный поиск -->
			<?php
				require_once "blocks/fssearch.php"
			?>
		<!--Конец Полноэкранного поиска -->
	</div>
	<!-- Страница-->
	<main role="main" id="MainContent">
	  	<!-- Шапка сортировки-->
	  		<?php
			//	require_once "blocks/floathead.php"
				require_once "blocks/adminusersnav.php"
			?>
		<!-- Конец Шапки сортировки-->
		<div class=" container ">
		<!--Основная часть страницы-->
		
		  	<!--Список добавление чела-->
		  	<div class="row">
		  	    <div class="col l6 offset-l3">
		  	   <form method="POST" action="#" id="test" accept-charset="UTF-8">
				    <label for="VKID">VK id</label>
                    <input type="number"  name="VKID" id="VKID" value="<?php echo $_GET['VKID'];?>">
				    <label for="NAME">Имя</label>
                    <input type="text" placeholder="Имя Фамилия" name="NAME" id="NAME" value="<?php echo $_GET['name'];?>">
                    <input name="sex" id="male" type="radio" value=2 checked />
                    <label for="male">Муж</label>
                    <br>
                    <input name="sex" id="female"  type="radio" value=1 />
                    <label for="female">Жен</label>
                    <br>
                    <input type="submit" name="edit" class="btn" value="Изменить">
			    </form>
			        <br>
			    </div>
	        </div>
			<!--Пыха часть-->
			<?php
			    if (!empty($_POST['edit'])) {
                    if(AddPeople($_POST['NAME'], $_POST['VKID']))
			        {
			         echo "<script>function ready() {
                     Materialize.toast('Изменено');
                     }
                     document.addEventListener(\"DOMContentLoaded\", ready);</script>";
			        }
			        else  
			        {
			         echo "<script>function ready() {
                     Materialize.toast('Ошибка при редактировании!');
                     }
                     document.addEventListener(\"DOMContentLoaded\", ready);</script>";
			        }
			    }
			?>
		<!--Номера страниц-->
			<?php
				pages();
			?>  
		<!--Конец номеров страниц-->
		</div>
	</main>
	<!--Всплывающие окна-->
		<?php
			require_once "blocks/search.php"
		?>  
	<!--Конец всплавыющих окон-->
	<!--Подвал-->
		<?php
			require_once "blocks/footer.php"
		?>
	<!--Конец подвала-->
	<!-- Javascript -->
		<?php
			require_once "blocks/js.php"
		?>
		
	<!-- Конец Javascript -->	  
</body>
</html>
