<?php
//Подключение библиотек, запуск сессии 
	require_once "blocks/start.php";
	if (GetUser($_SESSION["user_id"])['Acceslevel'] < 3) {header("Location: index.php");}
?>
<!doctype html>
<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		КиноДруг - Жанры
	</title>
	<!-- Линки -->
	<?php
		require_once "blocks/links.php"
	?>
	<!-- Конец Линков -->
		<link href="images/Jamespeng-Movie-Trailer.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="images/Jamespeng-Movie-Trailer.ico" rel="icon" type="image/x-icon" />
</head>
<body id="home-page" class="template-collection ">    
	<div id="shopify-section-header" class="shopify-section">
		<!-- Навигация и заголовок -->
			<?php
				//require_once "blocks/bignav.php"
				require_once "blocks/adminnav.php"
			?>
		<!-- Моибильная навигация -->
			<?php
				require_once "blocks/mobnavadmin.php"
			?>
		<!-- Конец мобильной шапки -->
	</div>
	<!-- Страница-->
	<main role="main" id="MainContent">
		<div class=" container ">
		    <h2>Жанры</h2>
		   <?php 
		   if(!empty($_POST["EDIT"]))
		   {
		    $tosay = editJenre($_POST["entry"],$_POST["NAME"]);
		   }
		   else if (!empty($_POST["DEL"]))
		   {
		       $tosay = delJenre($_POST["entry"]);
		   }
		   else if (!empty($_POST["ADD"]))
		   {
		        $tosay = addJenre($_POST["NAME"]);
		   }
		   if (!empty($_POST['ADD']) || !empty($_POST['DEL']) || !empty($_POST['EDIT'])) {
                    
			         echo "<script>function ready() {
                     Materialize.toast('".$tosay."', 4000);
                     }
                     document.addEventListener(\"DOMContentLoaded\", ready);</script>";
			       
			        }
		   PrintJenres() ?>
       <div id="modalADD" class="modal">
           <form enctype="multipart/form-data" method="post" action=""  accept-charset="UTF-8">
           <div class="modal-content">
            <h4>Новый жанр</h4>
        	<label for="NAME">Название жанра</label>
            <input type="text" placeholder="Название" name="NAME" id="NAME" value="">
            </div>
            <div class="modal-footer">
             <input type="submit" name="ADD" class="modal-close btn waves-effect white waves-green btn-flat" value="Добавить">
             <a href="#!" class="modal-close waves-effect waves-red btn-flat">Отмена</a>
            </div>
            </form>
         </div>  
		</div>
	</main>
	<div class="fixed-action-btn">
			  <a class="btn-floating btn-large black" href="#modalADD">
			    <i class="large material-icons">add</i>
			  </a>
			</div>
	<!--Всплывающие окна-->
		<?php
			require_once "blocks/search.php"
		?>  
	<!--Конец всплавыющих окон-->
	<!--Подвал-->
		<?php
			require_once "blocks/footer.php"
		?>
	<!--Конец подвала-->
	<!-- Javascript -->
		<?php
			require_once "blocks/js.php"
		?>
			<script>
		 	document.addEventListener('DOMContentLoaded', function() {
		    var elems = document.querySelectorAll('.fixed-action-btn');
		    var instances = M.FloatingActionButton.init(elems, options);
		  	});
		</script>
	<!-- Конец Javascript -->	  
</body>
</html>
