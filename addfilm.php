<?php
//Подключение библиотек, запуск сессии 
	require_once "blocks/start.php";
	if (GetUser($_SESSION["user_id"])['Acceslevel'] < 3) {header("Location: index.php");}

?>
<!doctype html>
<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		КиноДруг - Добавить фильм
	</title>
	<!-- Линки -->
	<?php
		require_once "blocks/links.php"
	?>
	<!-- Конец Линков -->
</head>
<body id="home-page" class="template-collection ">    
	<div id="shopify-section-header" class="shopify-section">
		<!-- Навигация и заголовок -->
			<?php
			//	require_once "blocks/bignav.php"
				require_once "blocks/adminnav.php"
			?>
		<!-- Моибильная навигация -->
			<?php
				require_once "blocks/mobnavadmin.php"
			?>
		<!-- Конец мобильной шапки -->
	
	</div>
	<!-- Страница-->
	<main role="main" id="MainContent">
	        <?php
				require_once "blocks/admincontentnav.php"
			?>
		<div class=" container ">
		<!--Основная часть страницы-->
		
		  	<!--форма добавление фильма-->
		  	<h4>Добавить фильм</h4>
		  	<script src="ckeditor/ckeditor.js"></script>
		  	<?php
		  	if (!empty($_POST['ADD'])) {
              $Jenres = $_POST['Jenre'];
              if(empty($Jenres)) 
              {
                $tosay = 'Выберите хотя бы один жанр!';
              } 
              else
              {
                $uploaddir = 'films/';
        		$uploadfile = $uploaddir . basename($_FILES['poster']['name']);
        		if (!move_uploaded_file($_FILES['poster']['tmp_name'], $uploadfile))
        		{
        			$uploadfile = $_POST[path];
        			if ($uploadfile != "")
        			{	
        			$uploadfile = $_POST[path];
        			}
        			else {$uploadfile = 'images/default.jpg';}
        		}
                $tosay = AddFilm($_POST['NAME'], $_POST['date'], $_POST['Country'], $_POST['budget'], $_POST['fees'], $_POST['rating'],$_POST['time'],$_POST['Agerating'],$uploadfile,$_POST['text'],$Jenres);
              }
		  	}
            ?>
		  	 <form enctype="multipart/form-data" method="post" action=""  accept-charset="UTF-8">
				    <label for="NAME">Название фильма</label>
                    <input type="text" placeholder="Название фильма" name="NAME" id="NAME" value="">
                    <label for="date">Дата выхода в прокат</label>
                    <input type="date" name="date" id="date" value="">
                    <label for="budget">Бюджет фильма</label>
                    <input type="number" name="budget" id="budget" value="">
                    <label for="fees">Кассовые сборы</label>
                    <input type="number" name="fees" id="fees" value="">
                    <label for="rating">Оценка критиков</label>
                    <input type="number" name="rating" id="rating" value="">
                    <label for="time">Продолжительность</label>
                    <input type="time" name="time" id="time" value="">
                    <label for="Agerating">Возрастное ограничение</label>
                    <input type="number" name="Agerating" id="Agerating" value="">
                    <label for="Country">Страна</label>
                    <input type="text"  name="Country" id="Country" value="">
                    <?php $Genre = GetAll("Jenre");
                    echo '<div class="row">
		          		<h5>Жанры:</h5>
		          		<div class="col s12 l6">';
                        for ($i = 0; $i < count($Genre)/2; $i++)
                        {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$i]["entry"].'" id="Jenre'.$i.'" />
		      			    <label for="Jenre'.$i.'">'.$Genre[$i]["Name"].'</label>  
		      			    <br>';
                        }
                    echo '</div>';
                    echo '<div class="col s12 l6">';
                        for ($i = count($Genre)/2; $i < count($Genre); $i++)
                        {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$i]["entry"].'" id="Jenre'.$i.'" />
		      			    <label for="Jenre'.$i.'">'.$Genre[$i]["Name"].'</label>   
		      			    <br>';
                        }
                    echo '</div></div>';
                    ?>
                    <div class="file-field input-field">
                      <div class="btn">
                        <span>Выбрать изображение</span>
                        <input type="file"name="poster"
                		accept="image/*" 
                        id="poster">
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate" placeholder="Постер" type="text">
                      </div>
                    </div>
				    <label for="text">Описание</label>
                    <textarea name="text" id="editor" class="materialize-textarea"></textarea>
                    <br>
                    <input type="submit" name="ADD" class="btn" value="Добавить">
			    </form>
			    <script>
					 CKEDITOR.replace( 'editor' );
					</script>
			        <br>
	
			<!--Пыха часть-->
			<?php
			    if (!empty($_POST['ADD'])) {
                    
			         echo "<script>function ready() {
                     Materialize.toast('".$tosay."');
                     }
                     document.addEventListener(\"DOMContentLoaded\", ready);</script>";
			       
			        }
			?>
	
		</div>
	</main>
	<!--Всплывающие окна-->
		<?php
			require_once "blocks/search.php"
		?>  
	<!--Конец всплавыющих окон-->
	<!--Подвал-->
		<?php
			require_once "blocks/footer.php"
		?>
	<!--Конец подвала-->
	<!-- Javascript -->
		<?php
			require_once "blocks/js.php"
		?>
		
	<!-- Конец Javascript -->	  
</body>
</html>
