<?php
require_once "blocks/start.php";
if (isset($_POST['mark']) && isset($_POST['user']) && isset($_POST['film']) && isset($_SESSION["user_id"])) 
{
    if ($_POST['mark'] > 5) $mark = 5;
    else if ($_POST['mark'] < 1) $mark = 1;
    else $mark = $_POST['mark'];
    if (isset($_SESSION["lastfilm"]))
    {
        if ($_POST['film'] != $_SESSION["lastfilm"])
        {
            $_SESSION["lastfilm"] = $_POST['film'];
            $_SESSION["mark"] = $_POST['mark'];
            if (AddRaiting($_SESSION["user_id"]*1,$_POST['film']*1,$mark*1))
            {
                echo json_encode(array('success' => 1));
            }
            else echo json_encode(array('success' => 2));
        }
        else 
        {
            if (isset($_SESSION["mark"]))
            {
                if ($_POST['mark'] != $_SESSION["mark"])
                {
                    sleep(1);
                    $_SESSION["mark"] = $_POST['mark'];
                    if (AddRaiting($_SESSION["user_id"]*1,$_POST['film']*1,$mark*1))
                    {
                        echo json_encode(array('success' => 1));
                    }
                    else echo json_encode(array('success' => 2));
                }
                else
                {
                    echo json_encode(array('success' => 1));
                }
            }
            else
            {
                $_SESSION["mark"] = $_POST['mark'];
                if (AddRaiting($_SESSION["user_id"]*1,$_POST['film']*1,$mark*1))
                {
                    echo json_encode(array('success' => 1));
                }
                else echo json_encode(array('success' => 2));
            }
        }
    }
    else
    {
        $_SESSION["lastfilm"] = $_POST['film'];
        if (isset($_SESSION["mark"]))
        {
            if ($_POST['mark'] != $_SESSION["mark"])
            {
                sleep(1);
                $_SESSION["mark"] = $_POST['mark'];
                if (AddRaiting($_SESSION["user_id"]*1,$_POST['film']*1,$mark*1))
                {
                    echo json_encode(array('success' => 1));
                }
                else echo json_encode(array('success' => 2));
            }
            else
            {
                echo json_encode(array('success' => 1));
            }
        }
        else
        {
            $_SESSION["mark"] = $_POST['mark'];
            if (AddRaiting($_SESSION["user_id"]*1,$_POST['film']*1,$mark*1))
            {
                echo json_encode(array('success' => 1));
            }
            else echo json_encode(array('success' => 2));
        }
    }
} 
else 
{
    echo json_encode(array('success' => 2));
}
?>