<?php
//Подключение библиотек, запуск сессии 
	require_once "blocks/start.php";
	if (GetUser($_SESSION["user_id"])['Acceslevel'] < 3) {header("Location: index.php");}

?>
<!doctype html>
<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		КиноДруг
	</title>
	<!-- Линки -->
	<?php
		require_once "blocks/links.php"
	?>
	<!-- Конец Линков -->
</head>
<body id="home-page" class="template-collection ">    
	<div id="shopify-section-header" class="shopify-section">
		<!-- Навигация и заголовок -->
			<?php
				//require_once "blocks/bignav.php"
				require_once "blocks/adminnav.php"
			?>
		<!-- Моибильная навигация -->
			<?php
			//	require_once "blocks/mobnav.php"
			?>
		<!-- Конец мобильной шапки -->
		<!-- Полноэкранный поиск -->
			<?php
				//require_once "blocks/fssearch.php"
			?>
		<!--Конец Полноэкранного поиска -->
	</div>
	<!-- Страница-->
	<main role="main" id="MainContent">
	  	<!-- Шапка сортировки-->
	  		<?php
			//	require_once "blocks/floathead.php"
			    require_once "blocks/adminfeedbacknav.php"
			?>
		<!-- Конец Шапки сортировки-->
		<div class=" container ">

		<!--Номера страниц-->
			<?php
				pages();
			?>  
		<!--Конец номеров страниц-->
		</div>
	</main>
	<!--Всплывающие окна-->
		<?php
			require_once "blocks/search.php"
		?>  
	<!--Конец всплавыющих окон-->
	<!--Подвал-->
		<?php
			require_once "blocks/footer.php"
		?>
	<!--Конец подвала-->
	<!-- Javascript -->
		<?php
			require_once "blocks/js.php"
		?>
		
	<!-- Конец Javascript -->	  
</body>
</html>
