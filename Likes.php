<?php
require_once "blocks/start.php";
	if(isset($_GET["user"])) 
	    {
	             $uid = $_GET["user"];
		         $User = GetUser($uid*1);
		         $name = $User["Name"];
	    } 
	    else 
	    {
	       $uid = $_SESSION["user_id"];
	       $User = GetUser($uid*1);
		   $name = $User["Name"];
	    }
?>
<!doctype html>
<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		КиноДруг - Оценки
	</title>
	<!-- Линки -->
	<?php
		require_once "blocks/links.php"
	?>
	<!-- Конец Линков -->
	<script src="https://vk.com/js/api/openapi.js?160" type="text/javascript"></script>
		<link href="images/ic.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="images/ic.ico" rel="icon" type="image/x-icon" />
</head>
<body id="home-page" class="template-collection "> 
    <!-- Javascript -->
		<?php
			require_once "blocks/js.php"
		?>
	<!-- Конец Javascript -->
	<div id="shopify-section-header" class="shopify-section">
		<!-- Навигация и заголовок -->
			<?php
			if(isset($_GET["num"])) {$num = $_GET["num"]; $num = $num*1;}
					else {$num = 0;}
			
                 require_once "blocks/nav.php";
			?>
		<!-- Моибильная навигация -->
			<?php
				require_once "blocks/mobnav.php";
			?>
		<!-- Конец мобильной шапки -->
		<!-- Полноэкранный поиск -->
			<?php
				require_once "blocks/fssearch.php";
			?>
		<!--Конец Полноэкранного поиска -->
	</div>
	<!-- Страница-->
	<main role="main" id="MainContent">
		<div class=" container ">
		<h3>Оценки - <?php echo $User["Name"];?></h3>
		<!--Основная часть страницы-->
			<div class="gallery gallery-masonry row">
		  	<!--Список фильмов-->
				<?php
    				$per_page = 6;
    				$Films = UsersFilmsLim($uid,$num,$per_page);
    				if ($_SESSION["user_id"] == $uid)
    				{
    				    films($Films, 1, $User);
    				}
    				else
    				{
    				    films($Films, 2, $User);
    				}
				?>
		  	</div> 
			<!--Конец списка-->
		<!--Номера страниц-->
			<?php
			PrintPages(UsersFilmsLimCount($uid), $num, $per_page,$search='&user='.$uid)
			?>  
		<!--Конец номеров страниц-->
		</div>
	</main>
	<!--Всплывающие окна-->
		<?php
			require_once "blocks/search.php"
		?>  
	<!--Конец всплавыющих окон-->
	<!--Подвал-->
		<?php
			require_once "blocks/footer.php"
		?>
	<!--Конец подвала-->
		  
</body>
</html>
