<?php
	//Подключение библиотек, запуск сессии 
	require_once "blocks/start.php";
		if(isset($_GET["id"])) 
	    {
		         $User = GetUser($_GET["id"]*1);
		         $name = $User["Name"];
	    } else $name = '';
?>
<!doctype html>
<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		КиноДруг - <?php echo $name; ?>
	</title>
	<!-- Линки -->
	<?php
		require_once "blocks/links.php"
	?>
	<!-- Конец Линков -->
	<link href="images/ic.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="images/ic.ico" rel="icon" type="image/x-icon" />
</head>
<body id="home-page" class="template-collection ">  
	<?php
			require_once "blocks/js.php"
		?>
	<div id="shopify-section-header" class="shopify-section">
		<!-- Навигация и заголовок -->
			<?php
				require_once "blocks/nav.php"
			?>
		<!-- Моибильная навигация -->
			<?php
				require_once "blocks/mobnav.php"
			?>
		<!-- Конец мобильной шапки -->
		<!-- Полноэкранный поиск -->
			<?php
				require_once "blocks/fssearch.php"
			?>
		<!--Конец Полноэкранного поиска -->
	</div>
	<!-- Страница-->
	<main role="main" id="MainContent">
	    <?php 	if(isset($User)) 
	    { 
	    ?>
		<!-- история навигации -->
		<!--	<nav>
			    <div class="nav-wrapper blue">
			      	<div class="col l6">
				        <a href="index.php" class="breadcrumb">&nbsp;Главная</a>
				        <a href="user.php" class="breadcrumb"><?php echo $_SESSION["user_FN"]." ".$_SESSION["user_LN"]?></a>
				        <a href="Friends.php" class="breadcrumb">Друзья</a>
				        <a href="#!" class="breadcrumb"><?php echo $User["Name"]; ?></a>
			      	</div>
			    </div>
			</nav>  -->
		<div class="container ">
		<!--Основная часть страницы-->
	
			<!-- Описания друга -->
			<div class="card-panel grey lighten-5">
	          	<div class="row">
	          	    
	          		<!-- аватарка -->
		            <div class="col s12 l4">
		            	<img src="<?php echo $User["Photo"];?>" alt="" class="circle responsive-img"> 
		            </div>
		            <!-- ФИО -->
		            <div class="col s12 l6">
		           		<h2><?php echo $User["Name"]; ?></h2><br>
		           		<h5>Информация о профиле</h5>
		           		<?php 
		           		   /* $U = GetUser($_SESSION["user_id"]);
		           		    if ($U['Acceslevel'] <= 0) echo '<br>Забанен';
		           		    else if ($U['Acceslevel'] > 0 && $U['Acceslevel'] == 1) echo '<br>Пользователь';
		           		    else if ($U['Acceslevel'] > 0 && $U['Acceslevel'] == 2) echo '<br>Модератор';
		           		    else if ($U['Acceslevel'] > 0 && $U['Acceslevel'] == 3) echo '<br>Администратор';
		           		    else if ($U['Acceslevel'] > 0 && $U['Acceslevel'] == 4) echo '<br>Разработчик';*/
		           		    echo '<p style=" font-size: 17px">Оцененных фильмов: '.UsersFilmsLimCount($_GET["id"]*1);
		           		    echo '<br>Друзей: '.UserFrindsCount($_GET["id"]*1).'</p>';
		           		?>
		            </div>
		            </div>
	          	</div>
        	<!--</div>-->
        	<!-- Список фильмов -->
        	<h2>Оцененные фильмы</h2>
        	<div class="gallery gallery-masonry row">
		  	<!--Список фильмов-->
				<?php
						$Films = UsersFilmsLim($_GET["id"]*1,0,6);
							films($Films,2,$User);
				?>
			
		  	</div> 

		  	<a href="Likes.php?user=<?php echo $_GET["id"]*1 ?>" class="waves-effect waves-light btn">Полный список</a><br><br>
		  	<!--Плавающая кнопка поиска-->
	        <!--<div class="fixed-action-btn">
			  <a class="btn-floating btn-large black" href="#modalSearch">
			    <i class="large material-icons">search</i>
			  </a>
			</div>-->
			<?php
	    }
	    else echo "<h1>Такой страницы не существует!</h1>";
			?>
		</div>
	</main>
	<!--Всплывающие окна-->
		<?php
			require_once "blocks/search.php"
		?>  
	<!--Конец всплавыющих окон-->
	<!--Подвал-->
		<?php
			require_once "blocks/footer.php"
		?>
	<!--Конец подвала-->
	<!-- Javascript -->
		<script>
		 	document.addEventListener('DOMContentLoaded', function() {
		    var elems = document.querySelectorAll('.fixed-action-btn');
		    var instances = M.FloatingActionButton.init(elems, options);
		  	});
		</script>
	
		
	<!-- Конец Javascript -->	  
</body>
</html>
