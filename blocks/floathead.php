<nav class="filter-navbar color blue">
		  	<div class="categories-wrapper">
		    	<div class="categories-container">          
		        	<ul class="categories container" data-filter="variant">
			      		<li>
			      		 	<select name="type">
								<option selected value="all">Жанр</option><option value="1">Хорор</option>
							</select>
						</li> 
						<li>  
							<form>
						        <div class="input-field">
						          	<input id="search" type="search" required>
						          	<label class="label-icon" for="search">
						          		<i class="material-icons">search</i>
						          	</label>
						         	<i class="material-icons">close</i>
						        </div>
				      		</form>
			      		</li>
			        </ul>    
		    	</div>
		 	</div>
		</nav>