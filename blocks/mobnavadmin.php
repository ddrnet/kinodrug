<ul class="side-nav" id="nav-mobile">
            <li class="no-padding">
                <ul class="collapsible collapsible-accordion">
                    <li>
                        <a class="collapsible-header"><?php echo $_SESSION["user_FN"]." ".$_SESSION["user_LN"]?><i class="material-icons">arrow_drop_down</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="/user.php">Профиль</a></li>
        			            <?php if (GetUser($_SESSION["user_id"])['Acceslevel'] > 2) echo '
                                    <li><a href="/admin.php">Админ панель</a></li>';
                                ?>
        			            <li><a href="/log.php?e=1">Выйти</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </li>
			<li>
                <a href="/Comments.php">Комментарии</a>
            </li>  
    		<li>
                <a href="/Users.php">Пользователи</a>
            </li>  		  
            <li>
                <a href="/adminfilms.php">Фильмы</a>
            </li>
            <li>
                <a href="/Genres.php">Жанры</a>
            </li>  
            <li>
                <a href="/Peoples.php">Режиссеры</a>
            </li>  
	      	<li><a href="/index.php">Главная</a> </li>
</ul>