<nav class="nav-extended color  black accent-3">
			<!-- фон -->
		    <div class="nav-background ">
		    	<!-- изображение -->
		    	<div class="pattern active" style="background-image: url('/images/back.png');"></div>
		    </div> 
			<div class="nav-wrapper container" style="background-color: #191919 !important;">
				<!-- Лого -->
			<img src="/images/log1.png" class="responsive-img" alt="log" style="height: 60px; width: 55px; padding-right: 5px;
    padding-top: 10px;"> 
			    <a href="/" itemprop="url" class="brand-logo site-logo">КиноДруг</a> 
			    <!-- Иконка для мобильной навигации -->   
			    <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
				 <!-- Меню навигации -->
			    <ul class="right hide-on-med-and-down">
			        <li >
			        	<a href="/index.php" class="site-nav__link">Главная</a>
			        </li>          		  
					<li>
			            <a href="/Friends.php" id="customer_login_link">Друзья</a>
			        </li>  		  
			        <li>
			            <a href="/New.php" id="customer_login_link">Новинки</a>
			        </li>  	
					<li>
			            <a 
                       class="site-nav__link dropdown-button"
                       data-activates="br-dropdown"
                       data-belowOrigin="true"
                       data-constrainWidth="true"
                       data-hover="true">
			           <?php echo $_SESSION["user_FN"]." ".$_SESSION["user_LN"]?>
			           <i class="material-icons right">arrow_drop_down</i>
			           </a>
			           <ul id="br-dropdown" class="site-nav__submenu dropdown-content">
			                <li>
                              <a href="/user.php" class="site-nav__link">Профиль</a>
                            </li>
                        <?php if (GetUser($_SESSION["user_id"])['Acceslevel'] > 2) echo '
                            <li>
                              <a href="https://oauth.vk.com/authorize?client_id=6953232&amp;display=page&amp;redirect_uri=http://a0305961.xsph.ru/admin.php&amp;response_type=code" class="site-nav__link">Админ панель</a>
                            </li>';
                        ?>
                            <li >
                              <a href="/log.php?e=1" class="site-nav__link">Выйти</a>
                            </li>
                        </ul>
				    </li> 
					<li>
			        <a class="fullscreen-search" href="#"><i class="material-icons">search</i></a>
			      	</li>	  		  
			    </ul>
			    <!-- Конец Меню навигации -->
			</div>
			<!-- Большая шапка 
		   	<div class="nav-header center">
		      <!--<h3>КиноДруг</h3>
		      <div class="tagline">Твой лучший помщник при выборе фильмов!</div>
		      
		  	</div>
		Конец большой шапки -->
		</nav>
		<div class="slider">
        <ul class="slides">
          <li>
            <img src="/images/s1.jpg"> <!-- random image -->
            <div class="caption center-align">
              
            </div>
          </li>
          <li>
            <img src="/images/s2.png"> <!-- random image -->
            <div class="caption left-align">
              
            </div>
          </li>
          <li>
            <img src="/images/s3.jpg"> <!-- random image -->
            <div class="caption right-align">
              
            </div>
          </li>
          <li>
            <img src="/images/s4.png"> <!-- random image -->
            <div class="caption center-align">
              
            </div>
          </li>
        </ul>
      </div>