<?php
	//Подключение библиотек, запуск сессии 
	require_once "blocks/start.php";
	require_once "lib/recaptchalib.php";
?>
<!doctype html>
<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		КиноДруг - Напишите нам
	</title>
	<!-- Линки -->
	<?php
		require_once "blocks/links.php"
	?>
	<!-- Конец Линков -->
		<link href="images/ic.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="images/ic.ico" rel="icon" type="image/x-icon" />
</head>
<body id="home-page" class="template-collection ">  
    <script src='https://www.google.com/recaptcha/api.js'></script>
	<div id="shopify-section-header" class="shopify-section">
		<!-- Навигация и заголовок -->
			<?php
				require_once "blocks/nav.php"
			?>
		<!-- Моибильная навигация -->
			<?php
				require_once "blocks/mobnav.php"
			?>
		<!-- Конец мобильной шапки -->
		<!-- Полноэкранный поиск -->
			<?php
				require_once "blocks/fssearch.php"
			?>
		<!--Конец Полноэкранного поиска -->
	</div>
	<!-- Страница-->
	<main role="main" id="MainContent">
		<div class="container ">
		<!--Основная часть страницы-->	
 <?php
    $captcha = 0;
    if ($_POST['email'] && $_POST['text']) 
    {
        //captcha
        // ваш секретный ключ
        $secret = "6LdF56gUAAAAAP9wplaLC33oLb9dS0s4PKyulmai";
        // пустой ответ
        $response = null;
        // проверка секретного ключа
        $reCaptcha = new ReCaptcha($secret);
        if ($_POST["g-recaptcha-response"]) {
         
        $response = $reCaptcha->verifyResponse(
                $_SERVER["REMOTE_ADDR"],
                $_POST["g-recaptcha-response"]
            );
        }
        if ($response != null && $response->success)
        {
        	$Text = trim(htmlspecialchars($_POST['text']));
            $email = trim(htmlspecialchars($_POST['email']));
            if (mail("kinodruq@list.ru", "Письмо с сайта", "ФИО:".$_SESSION["user_FN"]." ".$_SESSION["user_LN"]." E-mail: ".$email , $Text.". \r\n"))
            {
               echo "<script>function ready() {
                             Materialize.toast('Ваше письмо отправленно!');
                             }
                             document.addEventListener(\"DOMContentLoaded\", ready);</script>"; 
            }
            else 
            {
                echo "<script>function ready() {
                             Materialize.toast('При отправке произошла ошибка!');
                             }
                             document.addEventListener(\"DOMContentLoaded\", ready);</script>"; 
            }
        }
        else
        {
            echo "<script>function ready() {
                             Materialize.toast('Проверьте каптчу!');
                             }
                             document.addEventListener(\"DOMContentLoaded\", ready);</script>"; 
            $captcha = 1;
        }
    }
?>
			<div class="card-panel grey lighten-5">
	          <h3 class="header ">Свяжитесь с нами!</h3>
<div class="row">
    <form class="col s12" action="" method="post">
          <div class="input-field inline col s12">
            <input id="email" name="email" type="email" class="validate" <?php if($captcha == 1) echo 'value = "'.$_POST['email'].'"'; ?>>
            <label for="email"  >Электронная почта</label>
          </div>
		<div class="input-field col s12">
          <textarea id="textarea1" name="text" class="materialize-textarea"><?php if($captcha == 1) echo $_POST['text']; ?></textarea>
          <label for="textarea1">Ваше сообщение</label>
        </div>
     	<label for="cap"><br>Подтвердите что вы не робот</label>
          <div  class="g-recaptcha" data-sitekey="6LdF56gUAAAAAPOPTsF4W-NX3GVxfgnnDzDes1dS"></div>
          <br>
        <button class="btn waves-effect waves-light" type="submit" name="action">Отправить
		<i class="material-icons right">send</i>
		</button>
		
    </form>
   
  </div>
        	</div>
        	         
			    </div>
		</div>
	</main>
	<!--Всплывающие окна-->
		<?php
			require_once "blocks/search.php"
		?>  
	<!--Конец всплавыющих окон-->
	<!--Подвал-->
		<?php
			require_once "blocks/footer.php"
		?>
	<!--Конец подвала-->
	<!-- Javascript -->
		<script>
		 	document.addEventListener('DOMContentLoaded', function() {
		    var elems = document.querySelectorAll('.fixed-action-btn');
		    var instances = M.FloatingActionButton.init(elems, options);
		  	});
		  	 	var instance2 = M.Tabs.init(el, {
                            swipeable: true
                            });
		</script>
		<?php
			require_once "blocks/js.php"
		?>
		
	<!-- Конец Javascript -->	  
</body>
</html>
