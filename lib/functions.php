<?php
	//подключение к бд
	$mysqli = false;
	function connectDB() {
		global $mysqli;
		$mysqli = new mysqli ("localhost", "YOURBD", "PASS", "USER");
		if ($mysqli->connect_errno) {
            echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
        }
		$mysqli->query("SET NAMES 'utf8'");
	}
	//отключение от бд
	function closeDB() {
		global $mysqli;
		$mysqli->close();
	}
	//конверация в массив с именноваными полями
	function resultSetToArray($result_set) {
		$array = array();
		while  (($row = $result_set->fetch_assoc()) != false)
			$array[] = $row;
		return $array;
	}
	//общие функции
	function GetById($table,$id) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT * FROM `$table` WHERE `id`='$id'");
		closeDB();
		return $result_set->fetch_assoc();
    }

    function AddFilms($text,$foto) {
		global $mysqli;
		$s1 = $text[0];
        $s2 = $text[1];
        $s3 = $text[2];
        $s4 = $text[3];
        connectDB();
        $user_success = $mysqli->query("INSERT INTO `Film` (`FilmName`, `DateOf`, `Country`, `Description`,'Poster') VALUES ('$s1', '$s2', '$s3', '$s4','$foto')");
	    closeDB();        
	    return $user_success;
	}
	 function AddFilm($Name, $date, $country, $budget, $fees,$rating,$time,$Agerating,$Poster,$Desc,$Jenres) 
	 {
	     global $mysqli;
	     connectDB();
	     $success = $mysqli->query("INSERT INTO `Film` (`FilmName`, `DateOf`, `Country`, `Description`, `Poster`, `KinoRating`, `film_time`, `Fees`, `Budget`, `AgeRating`) VALUES ('$Name', '$date', $country, '$Desc', '$Poster', $rating, $time, $fees, $budget, $Agerating)");
	     if (!$success) 
	     {
	         closeDB(); 
	         return 'Ошибка в добавлении фильма!';
	     }
	     else 
	     {
	       $filmID = $mysqli->query("SELECT entry FROM `Film` ORDER BY `Film`.`entry` DESC LIMIT 1")->fetch_assoc()["entry"];
	       $success = $mysqli->query("DELETE FROM `FilmJenre` WHERE `film_id` = '$filmID'");
	       foreach($Jenres as $Genre)
	       {
	       $success = $mysqli->query("INSERT INTO `FilmJenre` (`film_id`, `jenre_id`) VALUES ('$filmID', $Genre)");
	       if (!$success) 
    	     {
    	         closeDB(); 
    	         return 'Ошибка в добавлении жанра!';
    	     }
	       }
	     }
	     closeDB(); 
	     return 'Фильм добавлен!';
	}
	function SearchRaiting($user,$film)
	{
	    global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT * FROM `Rating` WHERE  user_id = '$user' AND film_id = '$film'");
		closeDB();
		if ($result_set->fetch_assoc()) return true;
		else return false;
	}
	function AddRaiting($user,$film,$raiting) {
		global $mysqli;
		if (SearchRaiting($user,$film))
		{
		   	connectDB();
            $success = $mysqli->query("UPDATE `Rating` SET `Grade` = '$raiting', `DateOfRating` = current_date WHERE  user_id = '$user' AND film_id = '$film'");
    	    closeDB();  
		}
		else 
		{
    		connectDB();
            $success = $mysqli->query("INSERT INTO `Rating` (`user_id`, `film_id`, `Grade`, `DateOfRating`) VALUES ('$user', '$film', '$raiting', current_date)");
    	    closeDB();     
		}
	    return $success;
	}
	function AddPref($vk_id,$Jenres) {
		global $mysqli;
        connectDB();
        $success = $mysqli->query("DELETE FROM `PREFERENCES` WHERE `user_id` = '$vk_id'");
        if (count($Jenres)>0)
        {
        foreach($Jenres as $Genre)
	       {
	           
	       $success = $mysqli->query("INSERT INTO `PREFERENCES` (`user_id`, `jenre_id`) VALUES ('$vk_id', '$Genre')");
	      
	       if (!$success) 
    	     {
    	         closeDB(); 
    	         return 'Ошибка в добавлении жанра!';
    	     }
	       }
        }
	    closeDB();        
	    return 'Изменения сохранены!';
	}
	function GetAll($table) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT * FROM `$table`");
		closeDB();
		return resultSetToArray($result_set);	
	}
	function GetAllCount($table) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT COUNT(0) FROM `$table`");
		closeDB();
		$result_set = $result_set->fetch_assoc();
		return $result_set["COUNT(0)"];	
	}
	function GetSome($table, $num, $per_page) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT * FROM `$table` LIMIT $num,$per_page");
		closeDB();
		return resultSetToArray($result_set);	
	}
	function GetFilmPeople($film) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("select c.entry, c.Name, b.Role from Film a join Involvement b on a.entry = b.film_id join People c on c.entry = b.people_id where a.entry = '$film';");
		closeDB();
		return resultSetToArray($result_set);	
	}
	function FindNameFilm($num, $per_page, $value) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT (select avg(Grade) from Rating where film_id = a.entry) as Mark, a.entry, a.FilmName, a.DateOf, a.Budget, a.Fees, a.KinoRating, a.film_time, a.AgeRating, a.Country, a.Poster, a.Description FROM Film a WHERE a.FilmName like '%$value%' LIMIT $num,$per_page");
		closeDB();
		return resultSetToArray($result_set);	
	}
	function FindNameFilmCount($value) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT COUNT(0) FROM `Film` WHERE FilmName like '%$value%'");
		closeDB();
		$result_set = $result_set->fetch_assoc();
		return $result_set["COUNT(0)"];	
	}
	function FindNamePeople($num, $per_page, $value) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("select * FROM People WHERE Name like '%$value%' LIMIT $num,$per_page");
		closeDB();
		return resultSetToArray($result_set);	
	}
	function FindNamePeopleCount($value) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT COUNT(0) FROM `People` WHERE Name like '%$value%'");
		closeDB();
		$result_set = $result_set->fetch_assoc();
		return $result_set["COUNT(0)"];	
	}
	function FindNameUser($num, $per_page, $value) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("select * FROM User WHERE Name like '%$value%' LIMIT $num,$per_page");
		closeDB();
		return resultSetToArray($result_set);	
	}
	function FindNameUserCount($value) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT COUNT(0) FROM `User` WHERE Name like '%$value%'");
		closeDB();
		$result_set = $result_set->fetch_assoc();
		return $result_set["COUNT(0)"];	
	}
	function FindNameAdminFilm($num, $per_page, $value) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("select * FROM Film WHERE FilmName like '%$value%' LIMIT $num,$per_page");
		closeDB();
		return resultSetToArray($result_set);	
	}
	function FindNameAdminFilmCount($value) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT COUNT(0) FROM `Film` WHERE FilmName like '%$value%'");
		closeDB();
		$result_set = $result_set->fetch_assoc();
		return $result_set["COUNT(0)"];	
	}
	function GetSomeSortDEC($table, $num, $per_page) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT * FROM `$table` ORDER BY id DESC LIMIT $num,$per_page");
		closeDB();
		return resultSetToArray($result_set);	
	}
	function AddPeople2($name, $date, $gender) {
		global $mysqli;
		connectDB();
	    if ($gender == "on") $gender = 1;
	    else $gender = 0;
		$success = $mysqli->query("INSERT INTO `People` (`Name`, `Sex`, `DateOfBirth`) VALUES ('$name', $gender, '$date')");
		
		closeDB();
		return $success;
	}
	 
	function CheckUser($vk_id) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT * FROM `User` WHERE  vk_id = '$vk_id'");
		closeDB();
		if ($result_set->fetch_assoc()) return true;
		else return false;
	}
	function CheckUserGenre($vk_id,$g_id) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT * FROM `PREFERENCES` WHERE  user_id = '$vk_id' AND jenre_id = '$g_id'");
		closeDB();
		if($result_set){
		if ($result_set->fetch_assoc()) return true;
		else return false;
		}
		else return false;
	}
	function CheckFilmGenre($f_id,$g_id) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT * FROM `FilmJenre` WHERE  film_id = '$f_id' AND jenre_id = '$g_id'");
		closeDB();
		if ($result_set->fetch_assoc()) return true;
		else return false;
	}
	function AddUser($vk_id, $firstname, $lastname, $Sex, $Photo) {
	    global $mysqli;
        connectDB();
        $user_success = $mysqli->query("INSERT INTO `User` (`vk_id`, `Name`, `Sex`, `Acceslevel`, `Photo`) VALUES ('$vk_id', '$firstname $lastname', $Sex, 1, '$Photo')");
	    closeDB();
	    return $user_success;
	}
	function CheckFriend($vk_id, $f_id) {
		global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT * FROM `FriendZone` WHERE  owner_id = '$vk_id' AND `user_id` = '$f_id'");
		closeDB();
		if ($result_set->fetch_assoc()) return true;
		else return false;
	}
	function AddUserFriends($FriendsData, $vk_id)
	{
	    global $mysqli;
	    /*foreach($FriendsData as $Friend)
	    {
	        AddUser( $Friend['id'], $Friend['first_name'],  $Friend['last_name'],  $Friend['sex'], $Friend['photo_200_orig']);
		}*/
		if(UserFrindsCount($vk_id) > 0)
		{		
				connectDB();
			   $success = $mysqli->query("DELETE FROM `FriendZone` WHERE `owner_id` = '$vk_id'");
			   closeDB();
		}
	    foreach($FriendsData as $Friend)
	    {
	        $id = $Friend['id'];
	        if(CheckUser($vk_id))
	        {
	        connectDB();
	        $success = $mysqli->query("INSERT INTO `FriendZone` (`owner_id`, `user_id`) VALUES ('$vk_id', '$id')");
	        closeDB();
	        }
	        
	    }
	    
	}
	function UserFilms($vk_id,$from,$to,$nopref)
	{
	    global $mysqli;
	    connectDB();
	    if ($nopref != 0)
	    {
	        $result_set = $mysqli->query("(select distinct (select avg(R.Grade) from Rating R join FriendZone F on R.user_id = F.user_id where R.film_id = c.entry and F.owner_id = a.user_id) as Mark, a.user_id, c.entry, c.FilmName, c.DateOf, c.Budget, c.Fees, c.KinoRating, c.film_time, c.AgeRating, c.Country, c.Poster, c.Description from PREFERENCES a join FilmJenre b on a.jenre_id = b.jenre_id join Film c on b.film_id = c.entry left join Rating d on a.user_id = d.user_id and c.entry = d.film_id where d.user_id is null and a.user_id = '$vk_id' order by Mark desc) union all (select distinct (select avg(R.Grade) from Rating R join FriendZone F on R.user_id = F.user_id where R.film_id = c.entry and F.owner_id = a.user_id) as Mark, a.user_id, c.entry, c.FilmName, c.DateOf, c.Budget, c.Fees, c.KinoRating, c.film_time, c.AgeRating, c.Country, c.Poster, c.Description from PREFERENCES a join FilmJenre b on a.jenre_id <> b.jenre_id join Film c on b.film_id = c.entry left join Rating d on a.user_id = d.user_id and c.entry = d.film_id where d.user_id is null and a.user_id = '$vk_id' and b.jenre_id not in (select jenre_id from PREFERENCES where user_id = '$vk_id') order by Mark desc) LIMIT $from, $to");
	    }
	    else 
	    {
	        $result_set = $mysqli->query("select distinct (select avg(R.Grade) from Rating R join FriendZone F on R.user_id = F.user_id where R.film_id = c.entry and F.owner_id = d.user_id) as Mark, d.user_id, c.entry, c.FilmName, c.DateOf, c.Budget, c.Fees, c.KinoRating, c.film_time, c.AgeRating, c.Country, c.Poster, c.Description from Film c, Rating d where c.entry not in (select film_id from Rating where user_id = '$vk_id') and d.user_id = '$vk_id' order by Mark desc LIMIT $from, $to");
	    }
	    closeDB();
	    return resultSetToArray($result_set);
	}
	function UserFilmsCount($vk_id)
	{
	    global $mysqli;
	    connectDB();
	    $result_set = $mysqli->query("select COUNT(0) from ((select distinct (select avg(R.Grade) from Rating R join FriendZone F on R.user_id = F.user_id where R.film_id = c.entry and F.owner_id = a.user_id) as Mark, a.user_id, c.entry, c.FilmName, c.DateOf, c.Budget, c.Fees, c.KinoRating, c.film_time, c.AgeRating, c.Country, c.Poster, c.Description from PREFERENCES a join FilmJenre b on a.jenre_id = b.jenre_id join Film c on b.film_id = c.entry left join Rating d on a.user_id = d.user_id and c.entry = d.film_id where d.user_id is null and a.user_id = '$vk_id' order by Mark desc) union all (select distinct (select avg(R.Grade) from Rating R join FriendZone F on R.user_id = F.user_id where R.film_id = c.entry and F.owner_id = a.user_id) as Mark, a.user_id, c.entry, c.FilmName, c.DateOf, c.Budget, c.Fees, c.KinoRating, c.film_time, c.AgeRating, c.Country, c.Poster, c.Description from PREFERENCES a join FilmJenre b on a.jenre_id <> b.jenre_id join Film c on b.film_id = c.entry left join Rating d on a.user_id = d.user_id and c.entry = d.film_id where d.user_id is null and a.user_id = '$vk_id' and b.jenre_id not in (select jenre_id from PREFERENCES where user_id = '$vk_id') order by Mark desc)) a;");
		closeDB();
		$result_set = $result_set->fetch_assoc();
		return $result_set["COUNT(0)"];	
	}
	function UserFilmsCountNoPref($vk_id)
	{
	    global $mysqli;
	    connectDB();
	    $result_set = $mysqli->query("select COUNT(0) from (select distinct (select avg(R.Grade) from Rating R join FriendZone F on R.user_id = F.user_id where R.film_id = c.entry and F.owner_id = d.user_id) as Mark, d.user_id, c.entry, c.FilmName, c.DateOf, c.Budget, c.Fees, c.KinoRating, c.film_time, c.AgeRating, c.Country, c.Poster, c.Description from Film c, Rating d where c.entry not in (select film_id from Rating where user_id = '$vk_id') and d.user_id = '$vk_id' order by Mark desc) a;");
		closeDB();
		$result_set = $result_set->fetch_assoc();
		return $result_set["COUNT(0)"];	
	}
	function UserFrinds($vk_id)
	{
	    global $mysqli;
	    connectDB();
	    $result_set = $mysqli->query("SELECT user_id FROM `FriendZone` WHERE owner_id = '$vk_id'");
	    closeDB();
	    return resultSetToArray($result_set);
	}
	function UserFrindsLim($vk_id,$from,$to)
	{
	    global $mysqli;
	    connectDB();
	    $result_set = $mysqli->query("SELECT user_id FROM `FriendZone` WHERE owner_id = '$vk_id' LIMIT $from, $to");
	    closeDB();
	    return resultSetToArray($result_set);
	}
	function UsersLim($from,$to)
	{
	    global $mysqli;
	    connectDB();
	    $result_set = $mysqli->query("SELECT * FROM `User` LIMIT $from, $to");
	    closeDB();
	    return resultSetToArray($result_set);
	}
	function UsersCount()
	{
	    global $mysqli;
	    connectDB();
	    $result_set = $mysqli->query("SELECT COUNT(0) FROM `User`");
	    closeDB();
	    if ($result_set) {
		$result_set = $result_set->fetch_assoc();
		return $result_set["COUNT(0)"];	
		}
		else return 0;
	}
	function CountUsersSex($sex)
	{
		global $mysqli;
	    connectDB();
	    $result_set = $mysqli->query("SELECT COUNT(0) FROM `User` WHERE Sex = $sex");
	    closeDB();
	    if ($result_set) {
		$result_set = $result_set->fetch_assoc();
		return $result_set["COUNT(0)"];	
		}
		else return 0;
	}
	function CountUsersAcces($Accses)
	{
		global $mysqli;
	    connectDB();
	    $result_set = $mysqli->query("SELECT COUNT(0) FROM `User` WHERE Acceslevel = $Accses");
	    closeDB();
	    if ($result_set) {
		$result_set = $result_set->fetch_assoc();
		return $result_set["COUNT(0)"];	
		}
		else return 0;
	}
	function editUser($entry,$Name, $vk_id, $Acceslevel,$sex) {
		global $mysqli;
		connectDB();
		$success = $mysqli->query("UPDATE `User` SET `Name` = '$Name', `vk_id` = '$vk_id', `Acceslevel` = '$Acceslevel', `Sex` = '$sex' WHERE `entry` = '$entry'");
		closeDB();
		return $success;
	}
	function addJenre($Name) {
		global $mysqli;
		connectDB();
		$success = $mysqli->query("INSERT INTO `Jenre` (`Name`) VALUES ('$Name')");
		closeDB();
		if ($success) return "Жанр добавлен!";
		else return "Жанр не может быть добавлен!";
	}
	function editJenre($entry,$Name) {
		global $mysqli;
		connectDB();
		$success = $mysqli->query("UPDATE `Jenre` SET `Name` = '$Name' WHERE `Jenre`.`entry` = '$entry'");
		closeDB();
		if ($success) return "Жанр изменен!";
		else return "Жанр не может быть изменен!";
	}
	function delJenre($entry) {
		global $mysqli;
		connectDB();
		$success = $mysqli->query("DELETE FROM `Jenre` WHERE `Jenre`.`entry` = '$entry'");
		closeDB();
		if ($success) return "Жанр удален!";
		else return "Жанр не может быть удален!";
	}
	function PrintUsers($Users)
	{
	    if (count($Users)%2 == 1) $med = count($Users)/2+1;
        else $med = count($Users)/2;
	    echo '<div class="row">';
	    echo '<div class="col s12 l6"><ul class="collection">';
	    for($i = 0; $i < count($Users)/2; $i++)
	    {
	     echo '
	    <li class="collection-item avatar">
	    	<img src="'.$Users[$i]["Photo"].'" alt="" class="circle">
	    	
      			<span class="title">'.$Users[$i]["Name"].'</span>
       			<p>
       				<a href="https://vk.com/id'.$Users[$i]["vk_id"].'" target="_blank" class="waves-effect waves-light btn">Страница ВК</a> 
	         		<a href="friend.php?id='.$Users[$i]["vk_id"].'" class="waves-effect waves-light btn">Профиль</a><br><br>
	         		<a href="#modal'.$Users[$i]["vk_id"].'" class="wav``es-effect waves-light btn modal-trigger">Редактировать</a>
  				</p>
	      		<div id="modal'.$Users[$i]["vk_id"].'" class="modal modal-fixed-footer">
	      		<form method="POST" action="#" id="test" accept-charset="UTF-8">
                <div class="modal-content">
               
                   
                    <img src="'.$Users[$i]["Photo"].'" alt=""><h4>'.$Users[$i]["Name"].'</h4>
                    <input type="hidden" name="entry" value="'.$Users[$i]["entry"].'" />
				    <label  for="vk_id">VK id</label>
                    <input type="number"  name="vk_id" id="vk_id" readonly value="'.$Users[$i]["vk_id"].'">
				    <label for="Name">Имя</label>
                    <input type="text" required placeholder="Имя Фамилия" name="Name" id="Name" value="'.$Users[$i]["Name"].'">
                    <label for="Acceslevel">Уровень доступа</label>
                    <input class="validate"'; if ($Users[$i]['Acceslevel'] == 4 && GetUser($_SESSION["user_id"])['Acceslevel'] != 4) {echo 'readonly ';} echo 'required type="number"  name="Acceslevel" id="Acceslevel" value="'.$Users[$i]["Acceslevel"].'">
                    <input name="sex" id="male'.$Users[$i]["vk_id"].'" type="radio" value=2'; if ($Users[$i]["Sex"] == "2"){ echo ' checked';} echo ' />
                    <label for="male'.$Users[$i]["vk_id"].'">Муж</label>
                    <br>
                    <input name="sex" id="female'.$Users[$i]["vk_id"].'"  type="radio" value=1'; if ($Users[$i]["Sex"] == "1"){ echo ' checked';} echo ' />
                    <label for="female'.$Users[$i]["vk_id"].'">Жен</label>
                    <br>
                    
                </div>
                <div class="modal-footer">
                <input type="submit" name="edit" class="waves-effect white waves-green btn-flat" value="Изменить">
			    
                  <a href="#!" class="modal-close waves-effect waves-red btn-flat">Отмена</a>
                </div>
                </form>
              </div>
	    </li>';
		}
		
	    echo '</div><div class="col s12 l6"><ul class="collection">';
	    for($i = $med; $i < count($Users); $i++)
	    {
	     echo '
	    <li class="collection-item avatar">
	    	<img src="'.$Users[$i]["Photo"].'" alt="" class="circle">
	    	
      			<span class="title">'.$Users[$i]["Name"].'</span>
       			<p>
       				<a href="https://vk.com/id'.$Users[$i]["vk_id"].'" target="_blank" class="waves-effect waves-light btn">Страница ВК</a> 
	         		<a href="friend.php?id='.$Users[$i]["vk_id"].'" class="waves-effect waves-light btn">Профиль</a><br><br>
	         		<a href="#modal'.$Users[$i]["vk_id"].'" class="wav``es-effect waves-light btn modal-trigger">Редактировать</a>
  				</p>
	      		<div id="modal'.$Users[$i]["vk_id"].'" class="modal modal-fixed-footer">
	      		<form method="POST" action="#" id="test" accept-charset="UTF-8">
                <div class="modal-content">
               
                   
                    <img src="'.$Users[$i]["Photo"].'" alt=""><h4>'.$Users[$i]["Name"].'</h4>
                    <input type="hidden" name="entry" value="'.$Users[$i]["entry"].'" />
				    <label  for="vk_id">VK id</label>
                    <input type="number"  name="vk_id" id="vk_id" readonly value="'.$Users[$i]["vk_id"].'">
				    <label for="Name">Имя</label>
                    <input type="text" required placeholder="Имя Фамилия" name="Name" id="Name" value="'.$Users[$i]["Name"].'">
                    <label for="Acceslevel">Уровень доступа</label>
                    <input class="validate"'; if ($Users[$i]['Acceslevel'] == 4 && GetUser($_SESSION["user_id"])['Acceslevel'] != 4) {echo 'readonly ';} echo 'required type="number"  name="Acceslevel" id="Acceslevel" value="'.$Users[$i]["Acceslevel"].'">
                    <input name="sex" id="male'.$Users[$i]["vk_id"].'" type="radio" value=2'; if ($Users[$i]["Sex"] == "2"){ echo ' checked';} echo ' />
                    <label for="male'.$Users[$i]["vk_id"].'">Муж</label>
                    <br>
                    <input name="sex" id="female'.$Users[$i]["vk_id"].'"  type="radio" value=1'; if ($Users[$i]["Sex"] == "1"){ echo ' checked';} echo ' />
                    <label for="female'.$Users[$i]["vk_id"].'">Жен</label>
                    <br>
                    
                </div>
                <div class="modal-footer">
                <input type="submit" name="edit" class="waves-effect white waves-green btn-flat" value="Изменить">
			    
                  <a href="#!" class="modal-close waves-effect waves-red btn-flat">Отмена</a>
                </div>
                </form>
              </div>
	    </li>';
		}
		echo '</div></div>';
	}
	function FilmsLim($from,$to)
		{
			global $mysqli;
			connectDB();
			$result_set = $mysqli->query("SELECT * FROM `Film` LIMIT $from, $to");
			closeDB();
			return resultSetToArray($result_set);
		}
	function UsersFilmsLim($user, $from,$to)
		{
			global $mysqli;
			connectDB();
			$result_set = $mysqli->query("select distinct (select avg(Grade) from Rating where film_id = b.entry) as Mark, a.Grade, b.entry, b.FilmName, b.DateOf, b.Budget, b.Fees, b.KinoRating, b.film_time, b.AgeRating, b.Country, b.Poster, b.Description from Rating a join Film b on a.film_id = b.entry where a.user_id = '$user' LIMIT $from, $to");
			closeDB();
			return resultSetToArray($result_set);
		}
	function UsersFilmsLimCount($user)
		{
			global $mysqli;
			connectDB();
			$result_set = $mysqli->query("select COUNT(0) from `Rating` where user_id = '$user'");
			closeDB();
    	    if ($result_set) {
    		$result_set = $result_set->fetch_assoc();
    		return $result_set["COUNT(0)"];	
    		}
    		else return 0;
		}
	function FilmsLimNew($from,$to)
		{
			global $mysqli;
			connectDB();
			$result_set = $mysqli->query("select distinct (select avg(Grade) from Rating where film_id = b.entry) as Mark, b.entry, b.FilmName, b.DateOf, b.Budget, b.Fees, b.KinoRating, b.film_time, b.AgeRating, b.Country, b.Poster, b.Description from Rating a right join Film b on a.film_id = b.entry where a.user_id is null order by b.DateOf desc LIMIT $from, $to");
			closeDB();
			return resultSetToArray($result_set);
		}
	function UserFilmsRaited($user_id,$film_id)
		{
			global $mysqli;
			connectDB();
			$result_set = $mysqli->query("select b.Grade, b.user_id, c.Name, c.Photo from FriendZone a join Rating b on a.user_id = b.user_id join User c on c.vk_id = a.user_id where a.owner_id= '$user_id' and b.film_id = '$film_id';");
			closeDB();
			return resultSetToArray($result_set);
		}
    //Отрисовка фильмов в админке
    function PrintFilms($Films)
	{
	    echo '<div class="row">';
	    $Genre = GetAll("Jenre");
	    echo '<div class="col s12 l6"><ul class="collection">';
	    if (count($Films)%2 == 1) $medium = count($Films)/2+1;
	    else $medium = count($Films)/2;
	    for($i = 0; $i < count($Films)/2; $i++)
	    {
	    echo '
		<li class="collection-item avatar">
	    	<img src="'.$Films[$i]["Poster"].'" alt="" class="circle materialboxed">
	    	
      			<span class="title">'.$Films[$i]["FilmName"].'</span>
       		    <br><br>
                  <form enctype="multipart/form-data" method="post" action=""  accept-charset="UTF-8">
                  <input type="submit" data-target="moda'.$Films[$i]["entry"].'" value="Удалить" class="btn red z-depth-0modal-trigger">
                  <input type="submit" data-target="modal'.$Films[$i]["entry"].'" value="Редактировать" class="btn  z-depth-0modal-trigger">
        
                  <input type="hidden" name="entry" value="'.$Films[$i]["entry"].'" />
                <div id="moda'.$Films[$i]["entry"].'" class="modal">
                <div class="modal-content">
                  <h4>Удаление</h4>
                  <p>Вы уверены что хотите удалить фильм?</p>
                </div>
                <div class="modal-footer">
                <input type="submit" name="DEL" value="Удалить" class="btn red  modal-trigger">
                </div>
            </div>
                </form>
	      		<div id="modal'.$Films[$i]["entry"].'" class="modal modal-fixed-footer">
	      		<form enctype="multipart/form-data" method="post" action=""  accept-charset="UTF-8">
                <div class="modal-content">            
                   
                    <img src="'.$Films[$i]["Poster"].'" class="responsive-img materialboxed" width="350" alt=""><h4>'.$Films[$i]["FilmName"].'<h4>
				    <label for="FilmName">Название</label>
					<input  name="FilmName" id="FilmName" value="'.$Films[$i]["FilmName"].'">
                    <input type="hidden" name="entry" value="'.$Films[$i]["entry"].'" />
				    <label for="DateOf">Дата выхода</label>
					<input type="date" name="DateOf" id="DateOf" value="'.$Films[$i]["DateOf"].'">
                    <label for="Budget">Бюджет</label>
                    <input type="number"  name="Budget" id="Budget" value="'.$Films[$i]["Budget"].'">
                    <label for="Fees">Кассовые сборы</label>
                    <input type="number"  name="Fees" id="Fees" value="'.$Films[$i]["Fees"].'">
                    <label for="KinoRating">Рейтинг</label>
					<input type="number"  name="KinoRating" id="KinoRating" value="'.$Films[$i]["KinoRating"].'">
                    <label for="film_time">Продолжительность</label>
					<input type="time"  name="film_time" id="film_time" value="'.$Films[$i]["film_time"].'">
                    <label for="AgeRating">Возрастной рейтинг</label>
					<input type="number"  name="AgeRating" id="AgeRating" value="'.$Films[$i]["AgeRating"].'">
                    <label for="Country">Страна</label>
					<input  name="Country" id="Country" value="'.$Films[$i]["Country"].'">
                    <label for="Description">Описание</label>
                    <textarea  name="Description" id="Description'.$Films[$i]["entry"].'">';echo $Films[$i]["Description"];echo'</textarea>
                    ';
                    //$Genre = GetAll("Jenre");
                    echo '<h4>Жанры:</h4><div class="row">
		          		<div class="col s12 l6">';
		          		if (count($Genre)%2 == 1) $med = count($Genre)/2+1;
                	    else $med = count($Genre)/2;
                	    for($j = 0; $j < count($Genre)/2; $j++)
                        {
                            if(CheckFilmGenre($Films[$i]["entry"],$Genre[$j]["entry"]))
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$j]["entry"].'" id="'.$Films[$i]["entry"].'Jenre'.$j.'"  checked />
		      			    <label for="'.$Films[$i]["entry"].'Jenre'.$j.'">'.$Genre[$j]["Name"].'</label>  
		      			    <br>';
                            }
                            else 
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$j]["entry"].'" id="'.$Films[$i]["entry"].'Jenre'.$j.'" />
		      			    <label for="'.$Films[$i]["entry"].'Jenre'.$j.'">'.$Genre[$j]["Name"].'</label>  
		      			    <br>';  
                            }
                        }
                    echo '</div>';
                    echo '<div class="col s12 l6">';
                        for ($j = $med; $j < count($Genre); $j++)
                        {
                            if(CheckFilmGenre($Films[$i]["entry"],$Genre[$j]["entry"]))
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$j]["entry"].'" id="'.$Films[$i]["entry"].'Jenre'.$j.'" checked />
		      			    <label for="'.$Films[$i]["entry"].'Jenre'.$j.'">'.$Genre[$j]["Name"].'</label>  
		      			    <br>';
                            }
                            else 
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$j]["entry"].'" id="'.$Films[$i]["entry"].'Jenre'.$j.'" />
		      			    <label for="'.$Films[$i]["entry"].'Jenre'.$j.'">'.$Genre[$j]["Name"].'</label>  
		      			    <br>';  
                            }
                        }
                    echo '</div> </div> 
                    <h4>Загрузить новый постер</h4>
                      <div class="file-field input-field">
                      <div class="btn">
                        <span>Выбрать изображение</span>
                        <input type="file"name="poster" value=""
                		accept="image/*" 
                        id="poster">
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate" placeholder="Постер" type="text" name="path" value="'.$Films[$i]["Poster"].'">
                      </div>
                    </div>
                </div>
                <div class="modal-footer">
                <input type="submit" name="edit" class="waves-effect white waves-green btn-flat" value="Изменить">
			    
                  <a href="#" class="modal-close waves-effect waves-red btn-flat">Отмена</a>
                </div>
                </form>';
                
                echo "
                <script>
					 CKEDITOR.replace('Description".$Films[$i]['entry']."');
                    </script>";
                    
                    echo'
              </div>
	    </li>';
	    }
	    echo '</ul></div>';
	    echo '<div class="col s12 l6"><ul class="collection">';
	
	    for($i = $medium; $i < count($Films); $i++)
	    {
	    echo '
		<li class="collection-item avatar">
	    	<img src="'.$Films[$i]["Poster"].'" alt="" class="circle materialboxed">
	    	
      			<span class="title">'.$Films[$i]["FilmName"].'</span>
       		    <br><br>
                  <form enctype="multipart/form-data" method="post" action=""  accept-charset="UTF-8">
                  <input type="submit" data-target="moda'.$Films[$i]["entry"].'" value="Удалить" class="btn red z-depth-0modal-trigger">
                  <input type="submit" data-target="modal'.$Films[$i]["entry"].'" value="Редактировать" class="btn  z-depth-0modal-trigger">
        
                  <input type="hidden" name="entry" value="'.$Films[$i]["entry"].'" />
                <div id="moda'.$Films[$i]["entry"].'" class="modal">
                <div class="modal-content">
                  <h4>Удаление</h4>
                  <p>Вы уверены что хотите удалить фильм?</p>
                </div>
                <div class="modal-footer">
                <input type="submit" name="DEL" value="Удалить" class="btn red  modal-trigger">
                </div>
            </div>
                </form>
	      		<div id="modal'.$Films[$i]["entry"].'" class="modal modal-fixed-footer">
	      		<form enctype="multipart/form-data" method="post" action=""  accept-charset="UTF-8">
                <div class="modal-content">            
                   
                    <img src="'.$Films[$i]["Poster"].'" class="responsive-img materialboxed" width="350" alt=""><h4>'.$Films[$i]["FilmName"].'<h4>
				    <label for="FilmName">Название</label>
					<input  name="FilmName" id="FilmName" value="'.$Films[$i]["FilmName"].'">
                    <input type="hidden" name="entry" value="'.$Films[$i]["entry"].'" />
				    <label for="DateOf">Дата выхода</label>
					<input type="date" name="DateOf" id="DateOf" value="'.$Films[$i]["DateOf"].'">
                    <label for="Budget">Бюджет</label>
                    <input type="number"  name="Budget" id="Budget" value="'.$Films[$i]["Budget"].'">
                    <label for="Fees">Кассовые сборы</label>
                    <input type="number"  name="Fees" id="Fees" value="'.$Films[$i]["Fees"].'">
                    <label for="KinoRating">Рейтинг</label>
					<input type="number"  name="KinoRating" id="KinoRating" value="'.$Films[$i]["KinoRating"].'">
                    <label for="film_time">Продолжительность</label>
					<input type="time"  name="film_time" id="film_time" value="'.$Films[$i]["film_time"].'">
                    <label for="AgeRating">Возрастной рейтинг</label>
					<input type="number"  name="AgeRating" id="AgeRating" value="'.$Films[$i]["AgeRating"].'">
                    <label for="Country">Страна</label>
					<input  name="Country" id="Country" value="'.$Films[$i]["Country"].'">
                    <label for="Description">Описание</label>
                    <textarea  name="Description" id="Description'.$Films[$i]["entry"].'">';echo $Films[$i]["Description"];echo'</textarea>
                    ';
                    //$Genre = GetAll("Jenre");
                    echo '<h4>Жанры:</h4><div class="row">
		          		<div class="col s12 l6">';
		          		if (count($Genre)%2 == 1) $med = count($Genre)/2+1;
                	    else $med = count($Genre)/2;
                	    for($j = 0; $j < count($Genre)/2; $j++)
                        {
                            if(CheckFilmGenre($Films[$i]["entry"],$Genre[$j]["entry"]))
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$j]["entry"].'" id="'.$Films[$i]["entry"].'Jenre'.$j.'"  checked />
		      			    <label for="'.$Films[$i]["entry"].'Jenre'.$j.'">'.$Genre[$j]["Name"].'</label>  
		      			    <br>';
                            }
                            else 
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$j]["entry"].'" id="'.$Films[$i]["entry"].'Jenre'.$j.'" />
		      			    <label for="'.$Films[$i]["entry"].'Jenre'.$j.'">'.$Genre[$j]["Name"].'</label>  
		      			    <br>';  
                            }
                        }
                    echo '</div>';
                    echo '<div class="col s12 l6">';
                        for ($j = $med; $j < count($Genre); $j++)
                        {
                            if(CheckFilmGenre($Films[$i]["entry"],$Genre[$j]["entry"]))
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$j]["entry"].'" id="'.$Films[$i]["entry"].'Jenre'.$j.'" checked />
		      			    <label for="'.$Films[$i]["entry"].'Jenre'.$j.'">'.$Genre[$j]["Name"].'</label>  
		      			    <br>';
                            }
                            else 
                            {
                            echo '<input type="checkbox"  name="Jenre[]" class="filled-in" value="'.$Genre[$j]["entry"].'" id="'.$Films[$i]["entry"].'Jenre'.$j.'" />
		      			    <label for="'.$Films[$i]["entry"].'Jenre'.$j.'">'.$Genre[$j]["Name"].'</label>  
		      			    <br>';  
                            }
                        }
                    echo '</div> </div> 
                    <h4>Загрузить новый постер</h4>
                      <div class="file-field input-field">
                      <div class="btn">
                        <span>Выбрать изображение</span>
                        <input type="file"name="poster" value=""
                		accept="image/*" 
                        id="poster">
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate" placeholder="Постер" type="text" name="path" value="'.$Films[$i]["Poster"].'">
                      </div>
                    </div>
                </div>
                <div class="modal-footer">
                <input type="submit" name="edit" class="waves-effect white waves-green btn-flat" value="Изменить">
			    
                  <a href="#" class="modal-close waves-effect waves-red btn-flat">Отмена</a>
                </div>
                </form>';
                
                echo "
                <script>
					 CKEDITOR.replace('Description".$Films[$i]['entry']."');
                    </script>";
                    
                    echo'
              </div>
	    </li>';
	    }
	   echo '</ul></div>';
	    echo '</div>';
	}

	function editFilm($entry,$FilmName, $Poster, $DateOf,$Budget,$Fees,$KinoRating,$film_time,$AgeRating,$Country,$Description,$Jenres) {
		global $mysqli;
		connectDB();
		
		$success = $mysqli->query("UPDATE `Film` SET `FilmName` = '$FilmName', `Poster` = '$Poster', `DateOf` = '$DateOf', 
		`Budget` = $Budget, `Fees` = $Fees , `KinoRating` = $KinoRating , `film_time` = $film_time , `AgeRating` = $AgeRating , `Country` = $Country, `Description` = '$Description'   WHERE `entry` = '$entry'");
		if (!$success) 
	     {
	         closeDB(); 
	         return 'Ошибка в изменении фильма!';
	     }
	     else 
	     {
	       $success = $mysqli->query("DELETE FROM `FilmJenre` WHERE `film_id` = '$entry'");
	       foreach($Jenres as $Genre)
	       {
	       $success = $mysqli->query("INSERT INTO `FilmJenre` (`film_id`, `jenre_id`) VALUES ('$entry', $Genre)");
	       if (!$success) 
    	     {
    	         closeDB(); 
    	         return 'Ошибка в добавлении жанра!';
    	     }
	       }
	     }
	     closeDB(); 
		if ($success) return "Фильм изменен!";
		else return "Фильм не может быть изменен!";
	}
	    function deletefilm($entry) {
		global $mysqli;
		connectDB();
		$success = $mysqli->query("DELETE FROM `FilmJenre` WHERE `film_id` = '$entry'");
		$success = $mysqli->query("DELETE FROM `Involvement` WHERE `film_id` = '$entry'");
		$success = $mysqli->query("DELETE FROM `Rating` WHERE `film_id` = '$entry'");
		$success = $mysqli->query("DELETE FROM `Film` WHERE `entry` = '$entry'");
		closeDB();
		if ($success) return "Фильм удален!";
		else return "Фильм не может быть удален!";
	}
	function GetUser($vk_id)
	{
	 	global $mysqli;
		connectDB();
		$result_set = $mysqli->query("SELECT * FROM `User` WHERE `vk_id`='$vk_id'");
		closeDB();
		return $result_set->fetch_assoc();   
	}
	function PrintFriends($vk_id, $startpos=0)
	{
	    echo '<div class="row">';
	    $FriendsIds = UserFrindsLim($vk_id,$startpos,7);
	    echo '<div class="col s12 l6"><ul class="collection">';
	    foreach($FriendsIds as $Friend)
	    {
	    $User = GetUser($Friend["user_id"]);
	    echo '
	    <li class="collection-item avatar">
	    	<img src="'.$User["Photo"].'" alt="" class="circle">
      			<span class="title">'.$User["Name"].'</span>
       			<p>
       				<a href="https://vk.com/id'.$Friend["user_id"].'" target="_blank" class="waves-effect waves-light btn">Страница ВК</a> 
	         		<a href="friend.php?id='.$Friend["user_id"].'" class="waves-effect waves-light btn">Профиль</a>
  				</p>
	    </li>';
	    }
	    echo '</ul></div>';
	    $FriendsIds = UserFrindsLim($vk_id,$startpos+7,7);
	    echo '<div class="col s12 l6"><ul class="collection">';
	    foreach($FriendsIds as $Friend)
	    {
	    $User = GetUser($Friend["user_id"]);
	    echo '
	    <li class="collection-item avatar">
	    	<img src="'.$User["Photo"].'" alt="" class="circle">
      			<span class="title">'.$User["Name"].'</span>
       			<p>
       				<a href="https://vk.com/id'.$Friend["user_id"].'" target="_blank" class="waves-effect waves-light btn">Страница ВК</a> 
	         		<a href="friend.php?id='.$Friend["user_id"].'" class="waves-effect waves-light btn">Профиль</a>
  				</p>
	    </li>';
	    }
	    echo '</ul></div>';
	    echo '</div>';
	}
function PrintJenres()
	{
	    echo '<div class="row">';
	    $Jenres = GetAll('Jenre');
	    echo '<div class="col s12 l6"><ul class="collection">';
	    if (count($Jenres)%2 == 1) $med = count($Jenres)/2+1;
	    else $med = count($Jenres)/2;
	    for($i = 0; $i < count($Jenres)/2; $i++)
	    {
	    echo '
	    <li class="collection-item avatar">
	    <form enctype="multipart/form-data" method="post" action=""  accept-charset="UTF-8">
	    <input type="hidden" name="entry" value="'.$Jenres[$i]["entry"].'" />
       	<label for="NAME">Название жанра</label>
        <input type="text" placeholder="Название" name="NAME" id="NAME" value="'.$Jenres[$i]["Name"].'">
        <input type="submit" data-target="modal'.$Jenres[$i]["entry"].'" value="Удалить" class="btn red z-depth-0 modal-trigger">
        <input type="submit" name="EDIT" class="btn" value="Изменить">
        <div id="modal'.$Jenres[$i]["entry"].'" class="modal">
            <div class="modal-content">
              <h4>Удаление</h4>
              <p>Вы уверены что хотите удалить жанр?</p>
            </div>
            <div class="modal-footer">
            <a href="#" class="modal-close btn z-depth-0">Отмена</a>&nbsp
        	   <input type="submit" name="DEL" value="Подтверждаю" href="#modal1" class="btn red z-depth-0">&nbsp
            </div>
        </div>
       	</form>
	    </li>';
	    }
	    echo '</ul></div>';
	    echo '<div class="col s12 l6"><ul class="collection">';
	    for($i = $med; $i < count($Jenres); $i++)
	    {
	    echo '
	    <li class="collection-item avatar">
	    <form enctype="multipart/form-data" method="post" action=""  accept-charset="UTF-8">
	    <input type="hidden" name="entry" value="'.$Jenres[$i]["entry"].'" />
       	<label for="NAME">Название жанра</label>
        <input type="text" placeholder="Название" name="NAME" id="NAME" value="'.$Jenres[$i]["Name"].'">
        <input type="submit" data-target="modal'.$Jenres[$i]["entry"].'" value="Удалить" class="btn red z-depth-0 modal-trigger">
        <input type="submit" name="EDIT" class="btn" value="Изменить">
        <div id="modal'.$Jenres[$i]["entry"].'" class="modal">
            <div class="modal-content">
              <h4>Удаление</h4>
              <p>Вы уверены что хотите удалить жанр?</p>
            </div>
             <div class="modal-footer">
            <a href="#" class="modal-close btn z-depth-0">Отмена</a>&nbsp
        	   <input type="submit" name="DEL" value="Подтверждаю" href="#modal1" class="btn red z-depth-0">&nbsp
            </div>
        </div>
       	</form>
	    </li>';
	    }
	    echo '</ul></div>';
	    echo '</div>';
	}
	function UserFrindsCount($vk_id)
	{
	    global $mysqli;
	    connectDB();
	    $result_set = $mysqli->query("SELECT COUNT(0) FROM `FriendZone` WHERE owner_id = '$vk_id'");
	    closeDB();
	    if ($result_set) {
		$result_set = $result_set->fetch_assoc();
		return $result_set["COUNT(0)"];	
		}
		else return 0;
	}
	//отрисовка страниц
	function PrintPages($coun, $num, $per_page,$search='')
	{
	    if ($coun >= $per_page * 5)
	    {
    		echo '<ul class="pagination center-align">';
    		$lstn = $num - $per_page;
    		$ftrn = $num + $per_page;
    		$ind = 0;
    		$j = 0;
    		if ($num == 0) echo '<li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>';
    		else echo '<li class="waves-effect"><a href="'.$_SERVER['PHP_SELF'].'?num='.$lstn.$search.'"><i class="material-icons">chevron_left</i></a></li>';
    		if($coun > $per_page) 
    		{
        		for ($i = 0; $i < 2*$per_page; $i = $i + $per_page)
        		{
        			$ind++;
        			if ($num == $i) echo '<li class="active black"><a href="'.$_SERVER['PHP_SELF'].'?num='.$i.$search.'">'.$ind.'</a></li>';
        			else echo '<li class="waves-effect"><a href="'.$_SERVER['PHP_SELF'].'?num='.$i.$search.'">'.$ind.'</a></li>';
        		
        		}
        	
    		}
    		else 
    		{
    		   if ($num == 0) echo '<li class="active black"><a href="'.$_SERVER['PHP_SELF'].'?num=0'.$search.'">1</a></li>';
    			else echo '<li class="waves-effect"><a href="'.$_SERVER['PHP_SELF'].'?num=0'.$search.'">1</a></li>'; 
    		}
    		$ind = (int)(($num-3*$per_page)/$per_page);
    		
    		if ((int)(($coun-2*$per_page)/$per_page) > 3) 
    		{
    			if ($num-4*$per_page > $per_page)echo '<li class="waves-effect"><a href="'.$_SERVER['PHP_SELF'].'?num='.($num-4*$per_page).$search.'">...</a></li>';
        		for ($i = $num-3*$per_page; $i < $num+3*$per_page; $i = $i + $per_page)
        		{
        			$ind++;
        			
        			if ($ind > 2 && $ind <= (int)(($coun-2*$per_page)/$per_page))
        			{
        			if ($num == $i) echo '<li class="active black"><a href="'.$_SERVER['PHP_SELF'].'?num='.$i.$search.'">'.$ind.'</a></li>';
        			else echo '<li class="waves-effect"><a href="'.$_SERVER['PHP_SELF'].'?num='.$i.$search.'">'.$ind.'</a></li>';
        			}
        		}
        		if ($num+4*$per_page < $coun-2*$per_page) echo '<li class="waves-effect"><a href="'.$_SERVER['PHP_SELF'].'?num='.($num+4*$per_page).$search.'">...</a></li>';
    		}
    		$ind = (int)(($coun-2*$per_page)/$per_page);
    		if ($ind > 1) 
        	{
        	    
        		for ($i = $coun-2*$per_page; $i < $coun; $i = $i + $per_page)
        		{
        			$ind++;
        			
        			if ($num == $i) echo '<li class="active black"><a href="'.$_SERVER['PHP_SELF'].'?num='.$i.$search.'">'.$ind.'</a></li>';
        			else echo '<li class="waves-effect"><a href="'.$_SERVER['PHP_SELF'].'?num='.$i.$search.'">'.$ind.'</a></li>';
        			
        		}
    		}
    		
    		if ($ftrn >= $coun) echo '<li class="disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>';
    		else echo '<li class="waves-effect"><a href="'.$_SERVER['PHP_SELF'].'?num='.$ftrn.$search.'"><i class="material-icons">chevron_right</i></a></li>';
	    }
	    else 
    	{
    	    echo '<ul class="pagination center-align">';
    		$lstn = $num - $per_page;
			$ftrn = $num + $per_page;
			$ind = 0;
			if ($num == 0) echo '<li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>';
    		else echo '<li class="waves-effect"><a href="'.$_SERVER['PHP_SELF'].'?num='.$lstn.$search.'"><i class="material-icons">chevron_left</i></a></li>';
			for ($i = 0; $i < $coun; $i = $i + $per_page)
			{
				$ind++;
				if ($num == $i) echo '<li class="active black"><a href="'.$_SERVER['PHP_SELF'].'?num='.$i.$search.'">'.$ind.'</a></li>';
        			else echo '<li class="waves-effect"><a href="'.$_SERVER['PHP_SELF'].'?num='.$i.$search.'">'.$ind.'</a></li>';
				
			}
			if ($ftrn >=$coun) echo '<li class="disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>';
			else echo '<li class="waves-effect"><a href="'.$_SERVER['PHP_SELF'].'?num='.$ftrn.$search.'"><i class="material-icons">chevron_right</i></a></li>';
    	}
	}
	//функция выводит страницы фильмов
function films($Films,$vk=1,$User=NULL)
	{
	    $Genre = GetAll("Jenre"); 
		for ($i = 0; $i < count($Films); $i++)
		{
			echo '
			<!--Карта фильма-->
			<!--Теги для сортировки-->
			<div class="col s12 m4 gallery-item gallery-expand gallery-filter"
				data-type="1"
				data-section-type="product"
				data-color="1"
			  >
			<div class="gallery-curve-wrapper">
			   <!--Картинка-->
				  <a class="gallery-cover gray">
					<img 
					  src="'.$Films[$i]["Poster"].'"
					  crossorigin="anonymous"
					  width="189" height="400"
					 data-product-featured-image />
				  </a>
					<!--Лицевая часть карты-->
				  <div class="gallery-header">
					<span class="title">'.$Films[$i]["FilmName"].'</span>
					<div class="product-ratings-price">
					<div id="vk_like'.$Films[$i]["entry"].'"></div>
                    <script type="text/javascript">
                    VK.Widgets.Like("vk_like'.$Films[$i]["entry"].'", {type: "full"});
                    </script>
					</div>
				  </div>
			  <!--Тело карты-->
				<div class="gallery-body">
				  <div class="title-wrapper">
					<h3>'.$Films[$i]["FilmName"].'</h3>
				  </div>
				  <!--Атрибуты-->
					<div class="checkout-column">';
					foreach ($Genre as $Jenre) {
						if (CheckFilmGenre($Films[$i]["entry"],$Jenre["entry"])) {
							echo '<div class="chip">'.$Jenre["Name"].'</div>';
						}
					}
					if($Films[$i]["Grade"]) echo '<br>Оценка: '.$Films[$i]["Grade"].'';
					if($Films[$i]["DateOf"] != "0000-00-00" && $Films[$i]["DateOf"] != "1000-11-30") echo '<br>Дата премьеры: '.$Films[$i]["DateOf"].'';
					if($Films[$i]["Country"]) echo '<br>Страна: '.$Films[$i]["Country"].'';
					if($Films[$i]["film_time"]) echo '<br>Продолжительность: '.$Films[$i]["film_time"].'';
					if($Films[$i]["AgeRating"]) echo '<br>Возрастной рейтинг:'.$Films[$i]["AgeRating"].'';
					if($Films[$i]["Budget"]) echo '<br>Бюджет: '.$Films[$i]["Budget"].'';
					if($Films[$i]["Fees"]) echo '<br>Сборы: '.$Films[$i]["Fees"].'';
					if($Films[$i]["KinoRating"]) echo '<br><p class="flow-text">КиноПоиск: '.$Films[$i]["KinoRating"].'</p>';
					$peoples = GetFilmPeople($Films[$i]["entry"]);
					foreach ($peoples as $people) echo '<br> '.$people["Role"].' '.$people["Name"];
					echo '
					<div id="vk_like'.$Films[$i]["entry"].'a"></div>
                    <script type="text/javascript">
                    VK.Widgets.Like("vk_like'.$Films[$i]["entry"].'a", {type: "full"});
                    </script>';
                    $friends = UserFilmsRaited($_SESSION["user_id"],$Films[$i]["entry"]);
                    if ( count($friends) > 0) {
                    echo '<ul class="collapsible">
                    <li>
                      <div class="collapsible-header"><i class="material-icons">grade</i>Оценки друзей</div>
                      <div class="collapsible-body">
                      <ul class="collection">';
                      for ($j = 0; $j < count($friends); $j++) 
                    {
                       echo' <li class="collection-item avatar">
                          <img src="'.$friends[$j]["Photo"].'" alt="" class="circle">
                          <span class="title">'.$friends[$j]["Name"].'</span>
                          
                          <p class="secondary-content">'.$friends[$j]["Grade"].'</p>
                        </li>
                        
                       ';
                        
                    }
                    echo '
                    </ul>
                    </div>
                         </li>
                        </ul>';
                    }
					echo '</div>
				  <!--описание-->
					<div class="description">
						'.$Films[$i]["Description"];
						if ($vk==1) echo '<div id="vk_comments'.$Films[$i]["entry"].'"></div>';
						if ($vk==2) 
						{
						    $data = json_decode(file_get_contents('https://api.vk.com/method/widgets.getComments?widget_api_id=6953232&url=http://a0305961.xsph.ru&page_id='.$Films[$i]["entry"].'&access_token='.$_SESSION["access_token"].'&v=5.95'), true);
						    $data = $data['response']['posts'];
						    //echo '<br>'.$data['text'];
						    
	//					    if (count($data) > 0) {
    						    echo '<br>
    						    <ul class="collection with-header">
    						    <li class="collection-header"><h4>Отзывы пользователя:</h4></li>
    						    ';
    						    $limit = 5;
    						    for ($j = 0; $limit != 0 && $j < count($data); $j++)
    						    {
    						       if ($data[$j]['from_id'] == $User["vk_id"]) 
    						       {
    						        echo '<li class="collection-item avatar">
        						       <img src="'.$User["Photo"].'" alt="" class="circle">
                                        <span class="title">'.$User["Name"].'</span>
        						       <p>'.$data[$j]['text'].'</p>
        						       </li>'; 
        						       $limit--;
    						       }
    						       
    						    }
    						    echo '
    						    </ul>
    						    ';
						    }
						    
//						}
                    echo '    
					</div>
				</div>
				
				<div class="gallery-action">
				    <form id="loginform'.$Films[$i]["entry"].'" method="post">
				    <input type="hidden" name="film" value="'.$Films[$i]["entry"].'" />
				    <input type="hidden" name="user" value="'.$_SESSION["user_id"].'" />';
				    for ($j = 1.0; $j <= 5.0; $j += 1)
				    {
    				    if ($Films[$i]["Mark"]){
    				        if (floatval($Films[$i]["Mark"]) >= $j) echo '<button id="'.$j.'_'.$Films[$i]["entry"].'" type="submit" mark="'.$j.'" name="'.$j.'_'.$Films[$i]["entry"].'" value="1" class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star</i></button>';
    				        else if (floatval($Films[$i]["Mark"]) < $j && floatval($Films[$i]["Mark"]) > $j-0.5) echo '<button id="'.$j.'_'.$Films[$i]["entry"].'" type="submit" mark="'.$j.'" name="'.$j.'_'.$Films[$i]["entry"].'" value="1" class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star_half</i></button>';
    				        else echo '<button id="'.$j.'_'.$Films[$i]["entry"].'" type="submit" mark="'.$j.'" name="'.$j.'_'.$Films[$i]["entry"].'" value="'.$j.'" class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star_border</i></button>';
    				    }
    				    else echo '<button id="'.$j.'_'.$Films[$i]["entry"].'" type="submit" mark="'.$j.'" name="'.$j.'_'.$Films[$i]["entry"].'" value="1" class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">star_border</i></button>';
				    }
					
				    echo '</form>
				</div>  
				
				
			  </div>'  ;
		
	    for ($j = 1; $j <= 5; $j++)
        {
            echo 
            '
            <script type="text/javascript">
                $(document).ready(function() {
                    $(\'#loginform'.$Films[$i]["entry"].' #'.$j.'_'.$Films[$i]["entry"].'\').click(function(e) {
                        e.preventDefault();
                        $.ajax({
                            type: "POST",
                            url: \'check.php\',
                            data: $(this).parent().serialize() + \'&mark=\'+$(this).attr("mark"),
                            success: function(response)
                            {
                                var jsonData = JSON.parse(response);
                                if (jsonData.success == "1")
                                {
                                 Materialize.toast(\'Фильм оценен\',4000);
                                   
                                }
                                else
                                {
                                    Materialize.toast(\'Произошла ошибка!\',4000);
                                }
                           }
                       });
                     });
                });
                </script>
            ';
        }		
		echo '</div>
			<!--Конец карты-->
			';	
		}
		  echo '
	    <script type="text/javascript">
                        window.onload = function () {
                        VK.init({apiId: 6953232, onlyWidgets: true});';
		for ($i = 0; $i < count($Films); $i++)
		{
		   
                        	echo " VK.Widgets.Comments('vk_comments".$Films[$i]["entry"]."', {limit: 15}, ".$Films[$i]["entry"].");";
                        	echo " VK.Widgets.Like('vk_like".$Films[$i]["entry"]."a'";echo ',  {type: "full", height: 30}, '.$Films[$i]["entry"].');';
                        	echo " VK.Widgets.Like('vk_like".$Films[$i]["entry"]."'";echo ',  {type: "mini"}, '.$Films[$i]["entry"].');';
                     
		}
		 echo ' }
                        </script>';
        
		 
	}
	//прокаты
		function addPeople($Name) {
		global $mysqli;
		connectDB();
		$success = $mysqli->query("INSERT INTO `People` (`Name`) VALUES ('$Name')");
		closeDB();
		if ($success) return "Человек добавлен!";
		else return "Человек не может быть добавлен!";
	}
	function editPeople($entry,$Name) {
		global $mysqli;
		connectDB();
		$success = $mysqli->query("UPDATE `People` SET `Name` = '$Name' WHERE `People`.`entry` = '$entry'");
		closeDB();
		if ($success) return "Человек изменен!";
		else return "Человек не может быть изменен!";
	}
	function delPeople($entry) {
		global $mysqli;
		connectDB();
		$success = $mysqli->query("DELETE FROM `People` WHERE `People`.`entry` = '$entry'");
		closeDB();
		if ($success) return "Человек удален!";
		else return "Человек не может быть удален!";
	}
	function PrintPeoples($Peoples)
	{
	    echo '<div class="row">';
	    
	    echo '<div class="col s12 l6"><ul class="collection">';
	    if (count($Peoples)%2 == 1) $med = count($Peoples)/2+1;
	    else $med = count($Peoples)/2;
	    for($i = 0; $i < count($Peoples)/2; $i++)
	    {
	    echo '
	    <li class="collection-item avatar">
	    <form enctype="multipart/form-data" method="post" action=""  accept-charset="UTF-8">
	    <input type="hidden" name="entry" value="'.$Peoples[$i]["entry"].'" />
       	<label for="NAME">Имя человека</label>
        <input type="text" placeholder="Название" name="NAME" id="NAME" value="'.$Peoples[$i]["Name"].'">
        <input type="submit" data-target="modal'.$Peoples[$i]["entry"].'" value="Удалить" class="btn red z-depth-0 modal-trigger">
        <input type="submit" name="EDIT" class="btn" value="Изменить">
        <div id="modal'.$Peoples[$i]["entry"].'" class="modal">
            <div class="modal-content">
              <h4>Удаление</h4>
              <p>Вы уверены что хотите удалить режиссера?</p>
            </div>
            <div class="modal-footer">
            <a href="#" class="modal-close btn z-depth-0">Отмена</a>&nbsp
        	   <input type="submit" name="DEL" value="Подтверждаю" href="#modal1" class="btn red z-depth-0">&nbsp
            </div>
        </div>
       	</form>
	    </li>';
	    }
	    echo '</ul></div>';
	    echo '<div class="col s12 l6"><ul class="collection">';
	    for($i = $med; $i < count($Peoples); $i++)
	    {
	    echo '
	    <li class="collection-item avatar">
	    <form enctype="multipart/form-data" method="post" action=""  accept-charset="UTF-8">
	    <input type="hidden" name="entry" value="'.$Peoples[$i]["entry"].'" />
       	<label for="NAME">Имя человека</label>
        <input type="text" placeholder="Название" name="NAME" id="NAME" value="'.$Peoples[$i]["Name"].'">
        <input type="submit" data-target="modal'.$Peoples[$i]["entry"].'" value="Удалить" class="btn red z-depth-0 modal-trigger">
        <input type="submit" name="EDIT" class="btn" value="Изменить">
        <div id="modal'.$Peoples[$i]["entry"].'" class="modal">
            <div class="modal-content">
              <h4>Удаление</h4>
              <p>Вы уверены что хотите удалить человека?</p>
            </div>
             <div class="modal-footer">
            <a href="#" class="modal-close btn z-depth-0">Отмена</a>&nbsp
        	   <input type="submit" name="DEL" value="Подтверждаю" href="#modal1" class="btn red z-depth-0">&nbsp
            </div>
        </div>
       	</form>
	    </li>';
	    }
	    echo '</ul></div>';
	    echo '</div>';
	}
	
?>