<?php
require_once "blocks/start.php";
?>
<!doctype html>
<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		КиноДруг
	</title>
	<!-- Линки -->
	<?php
		require_once "blocks/links.php"
	?>
	<!-- Конец Линков -->
	<script src="https://vk.com/js/api/openapi.js?160" type="text/javascript"></script>
	<link href="images/ic.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="images/ic.ico" rel="icon" type="image/x-icon" />
</head>
<body id="home-page" class="template-collection "> 
<!-- Javascript -->
		<?php
			require_once "blocks/js.php"
		?>
	<!-- Конец Javascript -->	  
	<div id="shopify-section-header" class="shopify-section">
		<!-- Навигация и заголовок -->
			<?php
			    if (!empty($_POST['Genre'])) 
		          	 	{
                           	if (!empty($_POST['Jenre']))  $Jenres = $_POST['Jenre'];
                           	else $Jenres = [];
                            $tosay = AddPref($_SESSION["user_id"],$Jenres);
		          	 	}
				if(isset($_GET["num"])) {$num = $_GET["num"]; $num = $num*1;}
					else {$num = 0;}
				if ($num == 0)require_once "blocks/bignav.php";
				else require_once "blocks/nav.php";
			?>
		<!-- Моибильная навигация -->
			<?php
				require_once "blocks/mobnav.php";
			?>
		<!-- Конец мобильной шапки -->
		<!-- Полноэкранный поиск -->
			<?php
				require_once "blocks/fssearch.php";
			?>
		<!--Конец Полноэкранного поиска -->
	</div>
	<!-- Страница-->
	<main role="main" id="MainContent">
		<div class=" container ">
		<!--Основная часть страницы-->
			<div class="gallery gallery-masonry row">
		  	<!--Список фильмов-->
				<?php
				$search = '';
				$per_page = 6;
		        if (isset($_GET["qsr"])) 
		        {
		            if (FindNameFilmCount(htmlspecialchars($_GET['qsr'])) > 0) 
		            {
		              
		                $Films = FindNameFilm($num, $per_page, htmlspecialchars($_GET['qsr']));
		                $search = 'qsr='.$_GET["qsr"].'';
		                films($Films);
		            }
		            else echo "<h4>Поиск не дал результатов</h4>";
		        }
		        else 
		        {
		            $FilmsCount = UserFilmsCount($_SESSION["user_id"]*1);
		            $Films = UserFilms($_SESSION["user_id"]*1,$num,$per_page, $FilmsCount);
		            if ($FilmsCount == 0)   $FilmsCount = UserFilmsCountNoPref($_SESSION["user_id"]*1);
		            films($Films);
		        }
				?>
		  	</div> 
			<!--Конец списка-->
		<!--Номера страниц-->
			<?php
			if (isset($_GET["qsr"]) && FindNameFilmCount(htmlspecialchars($_GET['qsr'])) > 0)  PrintPages(FindNameFilmCount(htmlspecialchars($_GET['qsr'])), $num, $per_page,$search);
			else PrintPages($FilmsCount, $num, $per_page,$search)
			?>  
		<!--Конец номеров страниц-->
		</div>
	</main>
	<!--Всплывающие окна-->
		<?php
			require_once "blocks/search.php"
		?>  
	<!--Конец всплавыющих окон-->
	<!--Подвал-->
		<?php
			require_once "blocks/footer.php"
		?>
	<!--Конец подвала-->
	
</body>
</html>
