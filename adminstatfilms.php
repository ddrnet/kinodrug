<?php
		//Подключение библиотек, запуск сессии 
		require_once "blocks/start.php";
		if (GetUser($_SESSION["user_id"])['Acceslevel'] < 3) {header("Location: index.php");}
		?>
		<!doctype html>
		<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
		<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
		<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>
			КиноДруг - Панель администратора
		</title>
		<!-- Линки -->
		<?php
			require_once "blocks/links.php"
		?>
		<!-- Конец Линков -->
		</head>
		<body id="home-page" class="template-collection ">    
		<div id="shopify-section-header" class="shopify-section">
			<!-- Навигация и заголовок -->
				<?php
					//require_once "blocks/bignav.php"
					require_once "blocks/adminnav.php"
				?>
			<!-- Моибильная навигация -->
				<?php
				//	require_once "blocks/mobnav.php"
				?>
			<!-- Конец мобильной шапки -->
		</div>
		<!-- Страница-->
		<main role="main" id="MainContent">
			<div class=" container ">
				<div id="test" class="fixed-action-btn toolbar">
				<a class="btn-floating  btn-large black">
					<i class="large material-icons">menu</i>
				</a>
				<ul>
					<li><a href="/adminstatusers.php" class="btn black"><i class="material-icons larger">people</i></a></li>
					<li><a href="/adminstatfilms.php" class="btn black darken-1"><i class="material-icons larger">movie</i></a></li>
					<li><a href="/adminstatrating.php" class="btn black"><i class="material-icons larger">star_border</i></a></li>
					<li><a href="/admin.php" class="btn black"><i class="material-icons larger">menu</i></a></li>		    
				</ul>
				</div>
				<h5>Более подробная информация <a target="_blank" href="https://vk.com/stats?aid=6953232" class="btn-floating black"><i class="material-icons">camera_front</i></a> </h5>
				  <form action="/adminstatfilms.php" method="get" role="search">
						<select type="search" name="qsr1" for="search" >
						<option value=" " disabled selected>Тема поиска</option>
						<option value="FilmName">Название фильма</option>
						<option value="DateOf">Год произваодства</option>
						<option value="AgeRating">Возратсной рейтинг</option>
						<option value="Country">Страна</option>
						</select>
					  	<input type="search"
				         name="qsr"
				         id="Search"
						 placeholder="что ищем?">
					  	<button type="submit" class="btn waves-effect waves-light"> Поиск
						  	<svg aria-hidden="true" focusable="false" role="presentation" class="icon icon-search" viewBox="0 0 20 20"><path fill="#444" d="M18.64 17.02l-5.31-5.31c.81-1.08 1.26-2.43 1.26-3.87C14.5 4.06 11.44 1 7.75 1S1 4.06 1 7.75s3.06 6.75 6.75 6.75c1.44 0 2.79-.45 3.87-1.26l5.31 5.31c.45.45 1.26.54 1.71.09.45-.36.45-1.17 0-1.62zM3.25 7.75c0-2.52 1.98-4.5 4.5-4.5s4.5 1.98 4.5 4.5-1.98 4.5-4.5 4.5-4.5-1.98-4.5-4.5z"/></svg>
						    <span class="icon-fallback-text">Поиск</span>
					  	</button>
					</form>
					<?php 
					if (isset($_GET["qsr1"])) 
					{
						if ($_GET["qsr"] != "")
						{
							echo '				
							<div class="row">
								<div class="col s12 l2">
									<div class="card">
										<h1 align="center">'. GetAllCount('User') .'</h1>
										<center>Пользователи</center>
									</div>
								</div>				
								<div class="col s12 l2">
									<div class="card">
										<h1 align="center">'.GetAllCount('Film') .'</h1>
										<center>Фильмы</center>
									</div>
								</div>				
								<div class="col s12 l2">
									<div class="card">
										<h1 align="center">'.GetAllCount('People') .'</h1>
										<center>Режиссеры</center>
									</div>
								</div>				
							</div>';
						}
						else
						{
							echo "<h4>Вы не ввели данные поиска</h4>";
						}
					}
					else 
					{
						echo "<h4>Введите тему поиска</h4>";
					}
					?>

			</div>
		</main>
		<!--Всплывающие окна-->
			<?php
				require_once "blocks/search.php"
			?> 

		<!--Конец всплавыющих окон-->
		<!--Подвал-->
			<?php
				require_once "blocks/footer.php"
			?>
		<!--Конец подвала-->
		<!-- Javascript -->
			<?php
				require_once "blocks/js.php"
			?>
			<script>
			document.getElementById("test").addEventListener('DOMContentLoaded', function() {
			var elems = document.querySelectorAll('.fixed-action-btn');
			var instances = M.FloatingActionButton.init(elems, {
				toolbarEnabled: true
			});
			});
			document.addEventListener('DOMContentLoaded', function() {
			var elems = document.querySelectorAll('select');
			var instances = M.FormSelect.init(elems, options);
			});
			document.addEventListener('DOMContentLoaded', function() {
 			   var elems = document.querySelectorAll('.autocomplete');
 			   var instances = M.Autocomplete.init(elems, options);
 			 });
		</script>		
		<!-- Конец Javascript -->	  
		</body>
		</html>
