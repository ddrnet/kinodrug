<?php
	//Подключение библиотек, запуск сессии 
	require_once "blocks/start.php";
	if (GetUser($_SESSION["user_id"])['Acceslevel'] < 3) {header("Location: index.php");}
?>
<!doctype html>
<!--[if IE 9]> <html class="ie9 no-js supports-no-cookies" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js supports-no-cookies" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		КиноДруг - Пользователи
	</title>
	<!-- Линки -->
	<?php
		require_once "blocks/links.php"
	?>
	<!-- Конец Линков -->
		<link href="images/Jamespeng-Movie-Trailer.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="images/Jamespeng-Movie-Trailer.ico" rel="icon" type="image/x-icon" />
</head>
<body id="home-page" class="template-collection ">    
	<div id="shopify-section-header" class="shopify-section">
		<!-- Навигация и заголовок -->
			<?php
				require_once "blocks/adminnav.php"
			?>
		<!-- Моибильная навигация -->
			<?php
				require_once "blocks/mobnavadmin.php"
			?>
		<!-- Конец мобильной шапки -->
	</div>
	<!-- Страница-->
	<main role="main" id="MainContent">
		<div class="container ">
		<!--Основная часть страницы-->		
        	<div class="row">
			    <div class="col s12">
			        <h2>Список пользователей</h2>
			        <?php 
			        if(isset($_GET["num"])) {$num = $_GET["num"]; $num = $num*1;}
		            else {$num = 0;}
		            $per_page = 14;
		            $search = "";
			        if(!empty($_GET["qsr"]))
		            {
		                 $search = "&qsr=".$_GET["qsr"];
		                 $Users = FindNameUser($num, $per_page, $_GET["qsr"]);
		                 $UCount = FindNameUserCount($_GET["qsr"]);
		                 if ($UCount == 0) echo "<h4>Ничего не найдено!</h4>";
		            }
		            else
		            {
		                $Users = GetSome('User', $num, $per_page);
		                $UCount = GetAllCount('User');
		            }?>
    			    <?php 
    			    if(isset($_GET["num"])) {$num = $_GET["num"];}
    			    else {$num = 0;}
    			    if(!empty($_POST["edit"]) && ($_POST["Acceslevel"] != 4 || GetUser($_SESSION["user_id"])['Acceslevel'] == 4))  {
                    if(editUser($_POST["entry"], $_POST["Name"], $_POST["vk_id"], $_POST["Acceslevel"], $_POST["sex"]))
			        {
			         echo "<script>function ready() {
                     Materialize.toast('Изменено', 4000);
                     }
                     document.addEventListener(\"DOMContentLoaded\", ready);</script>";
			        }
			        else  
			        {
			         echo "<script>function ready() {
                     Materialize.toast('Ошибка при редактировании!', 4000);
                     }
                     document.addEventListener(\"DOMContentLoaded\", ready);</script>";
			        }
			    }
    			    PrintUsers($Users);
    			    PrintPages($UCount/*UsersCount()*/,$num ,$per_page,$search);
    			    ?>
			    </div>
        	</div>	         
		</div>
		  	<!--Плавающая кнопка поиска-->
		<div id="modalSearch" class="modal">
           <form enctype="multipart/form-data" method="get" action=""  accept-charset="UTF-8">
           <div class="modal-content">
            <h4>Поиск</h4>
        	<label for="NAME">Кого ищем?</label>
            <input type="text" name="qsr" id="qsr" value="">
            </div>
            <div class="modal-footer">
             <input type="submit" name="SEARCH" class="modal-close btn waves-effect white waves-green btn-flat" value="Искать">
             <a href="#!" class="modal-close waves-effect waves-red btn-flat">Отмена</a>
            </div>
            </form>
         </div>  
		</div>
	</main>
	<div class="fixed-action-btn">
			  <a class="btn-floating btn-large black" href="#modalSearch">
			    <i class="large material-icons">search</i>
			  </a>
			</div>
	<!--Всплывающие окна-->
		<?php
			require_once "blocks/search.php"
		?>  
	<!--Конец всплавыющих окон-->
	<!--Подвал-->
		<?php
			require_once "blocks/footer.php"
		?>
	<!--Конец подвала-->
	<!-- Javascript -->
		<?php
			require_once "blocks/js.php"
		?>
		
	<!-- Конец Javascript -->	  
</body>
</html>
